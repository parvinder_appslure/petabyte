import {AsyncStorageClear} from '../services/Api';
const data = {
  userDetail: {},
  userType: '',
  netInfo: {
    details: {},
    isConnected: false,
    isInternetReachable: false,
    isWifiEnabled: false,
    type: '',
  },
  deviceInfo: {
    id: '',
    token: '',
    model: '',
    os: '',
  },
  preferedTiming: [],
  masterStates: [],
};
const reducer = (state = data, action) => {
  console.log('action::', action.type);
  switch (action.type) {
    case 'userDetail':
      return {
        ...state,
        userDetail: action.payload.userDetail,
        userType: action.payload.userType,
      };
    case 'preferedTiming':
      return {
        ...state,
        preferedTiming: action.payload,
      };
    case 'masterStates':
      return {
        ...state,
        masterStates: action.payload,
      };
    case 'logout':
      AsyncStorageClear();
      return {
        ...state,
        userDetail: {},
        userType: '',
      };
    default:
      return state;
  }
};

export default reducer;

export const logout = () => ({type: 'logout'});
export const userDetail = payload => ({type: 'userDetail', payload});

export const preferedTiming = payload => ({type: 'preferedTiming', payload});
export const masterStates = payload => ({type: 'masterStates', payload});
export const SetDeviceInfo = deviceInfo => ({
  type: 'setDeviceInfo',
  payload: deviceInfo,
});
export const SetNetInfo = netInfo => ({type: 'setNetInfo', payload: netInfo});

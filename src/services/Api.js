// import AsyncStorage from '@react-native-community/async-storage';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {request, requestMultipart} from './ApiSauce';
// import Geolocation from '@react-native-community/geolocation';
// import Geocoder from 'react-native-geocoder';
import {Dimensions} from 'react-native';
import {GOOGLE_MAPS_APIKEY, latitudeDelta} from '../services/Config';
// const request = (path, json) => {
//   return new Promise((resolve, reject) => {
//     ApiSauce.post(path, json).then((response) => {
//       if (response.ok) {
//         resolve(response.data);
//       } else {
//         console.log(response.err);
//         reject(response.err);
//       }
//     });
//   });
// };

export const OtpApi = json => request('otp', json);
export const MPTApi = json => request('master_preferred_timing', json);
export const MSApi = json => request('master_state', json);
export const MBApi = json => request('master_boards', json);
export const MCApi = json => request('master_classes', json);
export const CDApi = json => request('course_details', json);
export const CourseSubjectApi = json => request('courses_subjects', json);
export const CourseChapterApi = json =>
  request('courses_subjects_chapters', json);
export const CourseChapterTopicApi = json =>
  request('course_study_topics_content', json);
export const CPaymentApi = json => request('add_permanent_booking', json);
export const SignInUserApi = json => request('signin', json);
export const SignUpUserApi = json => request('Signup', json);
export const FPApi = json => request('forget_password', json);
export const FPSApi = json => request('Forget_password_student', json);
export const CPApi = json => request('change_password', json);
export const GetProfileApi = json => request('get_profile', json);
export const LogoutApi = json => request('logout', json);
export const SignInStudentApi = json => request('Signin_student', json);
export const HomeStudentApi = json => request('home_student', json);
// export const DeleteMemberApi = json => request('delete_member', json);
// export const UpdateProfileApi = form => requestMultipart('edit_profile', form);

export const AsyncStorageSetUserId = json =>
  AsyncStorage.setItem('user_id', JSON.stringify(json));
export const AsyncStorageGetUserId = () => AsyncStorage.getItem('user_id');

export const AsyncStorageClear = () => AsyncStorage.clear();

export const AspectRatio = () =>
  Dimensions.get('window').width / Dimensions.get('window').height;
export const Height = Dimensions.get('window').height;
export const Width = Dimensions.get('window').width;
export const LongitudeDelta = () =>
  (latitudeDelta * Dimensions.get('window').width) /
  Dimensions.get('window').height;
export const LatitudeDelta = latitudeDelta;

export const formatAmount = amount =>
  `\u20B9 ${parseInt(amount)
    .toFixed(0)
    .replace(/(\d)(?=(\d\d)+\d$)/g, '$1,')}`;

export const formatNumber = str => str.replace(/,/g, '').replace('\u20B9 ', '');

export const textInPrice = price => `\u20B9 ${price}`;

export const timeFormate_mmss = time => {
  let mm = Math.floor(time / 60);
  let ss = time % 60;
  mm = mm < 10 ? `0${mm}` : mm;
  ss = ss < 10 ? `0${ss}` : ss;
  return `${mm}:${ss}`;
};

// export const GeolocationInfo = () => {
//   return new Promise((resolve, reject) => {
//     Geolocation.getCurrentPosition(info => {
//       console.log('info');
//       console.log(info);
//       resolve(info);
//     });
//   });
// };
// export const GeocoderLocation = ({latitude, longitude}) => {
//   return new Promise((resovle, reject) => {
//     Geocoder.geocodePosition({lat: latitude, lng: longitude}).then(res =>
//       resovle(res),
//     );
//   });
// };
// export const GoogleDirectionApi = (origin, destination) => {
//   console.log(origin);
//   console.log(destination);
//   return new Promise((resolve, reject) => {
//     const url = `https://maps.googleapis.com/maps/api/directions/json?origin=${origin}&destination=${destination}&mode=driving&units=metric&key=${GOOGLE_MAPS_APIKEY}`;
//     fetch(url, {
//       method: 'GET',
//       headers: {
//         'Content-Type': 'application/json',
//       },
//     })
//       .then(response => response.json())
//       .then(response => {
//         const {status, routes = []} = response;
//         if (status === 'OK' && routes.length) {
//           const {legs = []} = routes[0];
//           resolve({
//             status: true,
//             code: 200,
//             data: {
//               distance: legs[0].distance,
//               duration: legs[0].duration,
//             },
//           });
//         } else {
//           resolve({status: false, code: 204, data: response});
//         }
//       })
//       .catch(error => {
//         console.log('googledirectionapierror', error);
//         resolve({status: false, code: 500, data: error});
//       });
//   });
// };

export const getCouponModule = (condition = '', condition_outstation = '') => {
  console.log(condition, condition_outstation);
  switch (condition) {
    case 'from_airport':
      return 3;
    case 'to_airport':
      return 4;
    case 'local':
      return 5;
    case 'outstation':
      return 6;
    default:
      return 9;
  }
};

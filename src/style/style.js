import {Platform, StatusBar, StyleSheet} from 'react-native';

export const globStyle = StyleSheet.create({
  textInput: {
    marginTop: 10,
    width: '95%',
    alignSelf: 'flex-end',
    color: '#333A4D',
    fontSize: 17,
  },
});

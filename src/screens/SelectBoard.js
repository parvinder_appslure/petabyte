import React, {useEffect, useState} from 'react';

import {
  View,
  Text,
  Dimensions,
  StyleSheet,
  ImageBackground,
  Image,
  TouchableOpacity,
  ScrollView,
  TextInput,
  Modal,
} from 'react-native';

import {StatusBarLight} from '../utils/CustomStatusBar';

const {width, height} = Dimensions.get('window');

const SelectBoard = ({navigation}) => {
  return (
    <View style={styles.i_Style}>
      <StatusBarLight />
      <ImageBackground
        source={require('../images/bg.png')}
        style={styles.i_Style}>
        <ScrollView>
          <View style={styles.v_Style}>
            <Text style={styles.t_Style}>Select your School Boards</Text>

            <View style={{flexDirection: 'row', width: '90%', marginTop: 50}}>
              <TouchableOpacity
                onPress={() => navigation.navigate('SelectCourse')}
                style={{
                  backgroundColor: '#FFFFFF',
                  width: '30%',
                  borderRadius: 20,
                  marginLeft: 17,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Text
                  style={{
                    fontFamily: 'Avenir-Roman',
                    fontSize: 20,
                    fontWeight: '500',
                    //alignSelf: 'center',
                    color: '#F33F51',
                  }}>
                  CBSE
                </Text>
              </TouchableOpacity>

              <View
                style={{
                  backgroundColor: '#FFFFFF',
                  width: '60%',
                  borderRadius: 20,
                  padding: 10,
                  marginLeft: 20,
                }}>
                <Text
                  style={{
                    fontFamily: 'Avenir-Roman',
                    fontSize: 20,
                    fontWeight: '500',
                    alignSelf: 'center',
                    color: '#F33F51',
                  }}>
                  Haryana Board
                </Text>
              </View>
            </View>

            <View
              style={{
                backgroundColor: '#FFFFFF',
                width: '40%',
                borderRadius: 20,
                padding: 10,
                marginLeft: 20,
                marginTop: 20,
              }}>
              <Text
                style={{
                  fontFamily: 'Avenir-Roman',
                  fontSize: 20,
                  fontWeight: '500',
                  alignSelf: 'center',
                  color: '#F33F51',
                }}>
                ICSE Board
              </Text>
            </View>
          </View>
        </ScrollView>
      </ImageBackground>
    </View>
  );
};

export default SelectBoard;

const styles = StyleSheet.create({
  i_Style: {
    flex: 1,
  },
  v_Style: {
    width: '90%',
    alignSelf: 'center',
    //backgroundColor: 'yellow',
  },
  t_Style: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 34,
    color: '#FFFFFF',
    alignSelf: 'center',
    marginTop: 50,
  },
  t2_Style: {
    fontFamily: 'Avenir-Roman',
    fontSize: 20,
    fontWeight: '500',
    color: '#EF2B75',
  },
});

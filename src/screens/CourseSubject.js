import {relativeTimeThreshold} from 'moment';
import React, {useEffect, useState} from 'react';
import {
  Dimensions,
  TouchableOpacity,
  Text,
  StyleSheet,
  Image,
  View,
  ScrollView,
} from 'react-native';
import {CourseSubjectApi} from '../services/Api';
import {StatusBarLight} from '../utils/CustomStatusBar';
import {MainView, SimpleHeader, LinearView} from '../utils/Header';
import Loader from '../utils/Loader';

const {width, height} = Dimensions.get('window');
const CourseSubject = ({navigation, route}) => {
  const [state, setState] = useState({
    list: [],
    loading: true,
  });
  useEffect(() => {
    consolejson(route.params);
    (async () => {
      const response = await CourseSubjectApi(route.params);
      const {status = false, list = []} = response;
      !status && alert('Something went wrong');
      setState({...state, list, loading: false});
      consolejson(response);
    })();
  }, []);
  return (
    <MainView>
      <StatusBarLight />
      <Loader status={state.loading} />
      <SimpleHeader title={'Select Subject'} onPress={navigation.goBack} />
      <ScrollView>
        <View style={styles.viewSubject}>
          {state.list.map((item, index) => (
            <TouchableOpacity
              style={{margin: 10}}
              onPress={() => {
                navigation.navigate('CourseChapter', {
                  ...route.params,
                  subject_id: item.id,
                });
              }}>
              <LinearView style={styles.linearView} index={index}>
                <Image
                  source={require('../images/logo-2.png')}
                  style={styles.imageCourse}
                />
                <Text style={styles.textName}>{item.name}</Text>
              </LinearView>
            </TouchableOpacity>
          ))}
        </View>
      </ScrollView>
    </MainView>
  );
};

export default CourseSubject;

const styles = StyleSheet.create({
  linearView: {
    width: width / 2 - 30,
    height: width / 2 - 30,
    borderBottomRightRadius: 20,
    borderTopLeftRadius: 20,
    padding: 20,
    justifyContent: 'space-between',
  },
  imageCourse: {
    width: 60,
    height: 60,
    resizeMode: 'contain',
    alignSelf: 'flex-end',
  },
  textName: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 20,
    color: '#FFFFFF',
  },
  viewSubject: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    margin: 10,
  },
});

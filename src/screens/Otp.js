import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  Text,
  ImageBackground,
  Image,
  View,
  ScrollView,
  Dimensions,
  TouchableOpacity,
} from 'react-native';

import OTPInputView from '@twotalltotems/react-native-otp-input';

import {StatusBarLight} from '../utils/CustomStatusBar';
import {BackButton, BackgroundImage, H1, H2, MainView} from '../utils/Header';
import {
  AsyncStorageSetUserId,
  FPApi,
  FPSApi,
  OtpApi,
  SignUpUserApi,
} from '../services/Api';
import {GenerateOtp} from '../utils/CommonFunction';
import {useDispatch} from 'react-redux';
import * as actions from '../redux/actions';
const {width, height} = Dimensions.get('window');

const Otp = ({navigation, route}) => {
  const dispatch = useDispatch();
  const [state, setState] = useState({
    otp: '',
    loading: false,
  });
  useEffect(() => {
    consolejson(route.params);
  }, []);
  const onSubmitHandler = async () => {
    const {body, userType} = route.params;
    const {otp} = state;
    if (body.otp == otp) {
      if (['student', 'individual'].includes(userType)) {
        navigation.navigate('CreatePassword', {...route.params});
      }
      if (userType === 'individualSignUp') {
        const response = await SignUpUserApi(body);
        consolejson(response);
        const {
          status = false,
          msg = 'Something went wrong',
          user_detail = {},
        } = response;
        if (status) {
          dispatch(
            actions.userDetail({
              userDetail: user_detail,
              userType: 'individual',
            }),
          );
          AsyncStorageSetUserId({
            userId: user_detail.user_id,
            userType: 'individual',
          });
          navigation.reset({
            index: 0,
            routes: [{name: 'DrawerNavigator'}],
          });
        } else {
          alert(msg);
        }
      }
    } else {
      alert('Invalid Otp');
    }
  };
  const resendHandler = async () => {
    const {body, userType} = route.params;
    const otp = GenerateOtp();
    const resetOtp = (status, msg) => {
      if (status) {
        body.otp = otp;
        consolejson(otp);
        setState({...state, otp: ''});
      } else {
        alert(msg);
      }
    };
    if (userType === 'student') {
      const {status = false, msg = ''} = await FPSApi({...body, otp});
      resetOtp(status, msg);
      // if (status) {
      //   body.otp = otp;
      //   setState({...state, otp: ''});
      // } else {
      //   alert(msg);
      // }
    }
    if (userType === 'individual') {
      const {status = false, msg = ''} = await FPApi({...body, otp});
      resetOtp(status, msg);
      // if (status) {
      //   body.otp = otp;
      //   setState({...state, otp: ''});
      // } else {
      //   alert(msg);
      // }
    }
    if (userType === 'individualSignUp') {
      const {status = false, msg = ''} = await OtpApi({
        email: body.email,
        mobile: body.mobile,
        otp,
      });
      resetOtp(status, msg);
      // if (status) {
      //   body.otp = otp;
      //   setState({...state, otp: ''});
      // } else {
      //   alert(msg);
      // }
    }
  };
  const onCodeChanged = otp => setState({...state, otp});

  return (
    <MainView>
      <StatusBarLight />
      <BackgroundImage>
        <ScrollView>
          <BackButton onPress={navigation.goBack} />
          <H1 text={'Phone Verification'} />
          <H2 text={'Enter the OTP code sent to '} />
          <OTPInputView
            style={styles.OTPInputView}
            pinCount={4}
            code={state.otp} //You can supply this prop or not. The component will be used as a controlled / uncontrolled component respectively.
            onCodeChanged={onCodeChanged}
            autoFocusOnLoad
            codeInputFieldStyle={styles.underlineStyleBase}
            codeInputHighlightStyle={styles.underlineStyleHighLighted}
            onCodeFilled={code => {
              console.log(`Code is ${code}, you are good to go!`);
            }}
          />

          <TouchableOpacity style={styles.touchResend} onPress={resendHandler}>
            <Text style={styles.textResend}>{`Resend Otp Code ->`}</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={onSubmitHandler}
            style={styles.touchVerify}>
            <Text style={styles.tb2_Style}>Verify</Text>
          </TouchableOpacity>
        </ScrollView>
      </BackgroundImage>
    </MainView>
  );
};

export default Otp;

const styles = StyleSheet.create({
  i_Style: {
    flex: 1,
  },
  v_Style: {
    width: '90%',
    alignSelf: 'center',
  },
  i2_Style: {
    width: 40,
    height: 40,
    resizeMode: 'contain',
    position: 'absolute',
    marginTop: 40,
  },
  t_Style: {
    fontFamily: 'Avenir',
    fontSize: 34,
    fontWeight: 'bold',
    color: '#FFFFFF',
    marginTop: 100,
    alignSelf: 'center',
  },
  t2_Style: {
    fontFamily: 'Avenir',
    fontSize: 17,
    fontWeight: 'bold',
    color: '#FFFFFF',
    marginLeft: 22,
    marginTop: 10,
  },
  tb_Style: {
    fontFamily: 'Avenir',
    fontSize: 17,
    fontWeight: '500',
    color: '#FF2D55',
    marginRight: 20,
    alignSelf: 'flex-end',
    marginTop: 15,
  },
  tb2_Style: {
    fontFamily: 'Avenir',
    fontSize: 17,
    fontWeight: 'bold',
    color: '#EF2B75',
    alignSelf: 'center',
    justifyContent: 'center',
  },
  underlineStyleBase: {
    color: '#0A1F44',
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 30,
    width: 60,
    height: 60,
    borderWidth: 0,
    backgroundColor: 'white',
    borderRadius: 4,
  },

  underlineStyleHighLighted: {
    borderColor: '#cdcdcd',
  },
  OTPInputView: {
    height: 150,
    width: '80%',
    alignSelf: 'center',
  },
  touchVerify: {
    backgroundColor: '#FFFFFF',
    borderColor: 20,
    width: '80%',
    alignSelf: 'center',
    borderRadius: 25,
    padding: 15,
    marginTop: 50,
  },
  touchResend: {
    marginHorizontal: 40,
    alignSelf: 'flex-start',
    padding: 5,
    marginVertical: 20,
  },
  textResend: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '400',
    fontSize: 17,
    color: '#FFFFFF',
  },
});

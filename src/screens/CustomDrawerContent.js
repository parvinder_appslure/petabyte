import React from 'react';
import {
  StyleSheet,
  Image,
  Text,
  SafeAreaView,
  View,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {LinearHeader} from '../utils/Header';
import * as actions from '../redux/actions';
import {AsyncStorageClear} from '../services/Api';

const CustomDrawerContent = ({navigation}) => {
  const dispatch = useDispatch();
  const {userDetail} = useSelector(state => state);
  const OptionView = ({title, source, _key}) => (
    <TouchableOpacity
      style={styles.touchOption}
      onPress={() => optionHandler(_key)}>
      <Image source={source} style={styles.imageOption} />
      <Text style={styles.textOption}>{title}</Text>
    </TouchableOpacity>
  );

  const optionHandler = (key = '') => {
    console.log(key);
    if (key === '') return;
    switch (key) {
      case 'home':
        navigation.closeDrawer();
        break;
      case 'logout':
        AsyncStorageClear();
        dispatch(actions.logout());
        navigation.reset({
          index: 0,
          routes: [{name: 'Splash'}],
        });
        break;
      default:
        console.log('key', key);
        navigation.closeDrawer();
        navigation.navigate(key);
    }
  };
  return (
    <SafeAreaView style={styles.container}>
      <LinearHeader style={{flex: 1}}>
        <ScrollView>
          <View style={styles.viewProfile}>
            <Image
              source={require('../assets/v1.png')}
              style={styles.imageProfile}
            />
            <View style={styles.viewDetail}>
              <Text style={styles.textId}>{`ID : ${userDetail.user_id}`}</Text>
              <Text style={styles.textNumber}>{userDetail.mobile}</Text>
            </View>
          </View>
          <View style={styles.hzLine} />
          <OptionView
            title="Home"
            source={require('../assets/home.png')}
            _key={'home'}
          />
          <OptionView
            title="Language (English)"
            source={require('../assets/language.png')}
          />
          <OptionView
            title="Subscription (24 Days Left)"
            source={require('../assets/rupee-indian.png')}
            _key={'Subscriptions'}
          />
          <OptionView
            title="Revision (32 Cards Left)"
            source={require('../assets/clock.png')}
          />
          <OptionView
            title="Leader Board"
            source={require('../assets/leader.png')}
          />
          <OptionView
            title="My Groups"
            source={require('../assets/group.png')}
          />
          <OptionView
            title="Exam Dates"
            source={require('../assets/exam.png')}
            _key="ExamDate"
          />
          <OptionView
            title="Analysis"
            source={require('../assets/statistics.png')}
          />
          <OptionView
            title="Settings"
            source={require('../assets/settings.png')}
            _key="Settings"
          />
          <OptionView
            title="Logout"
            source={require('../assets/logout.png')}
            _key="logout"
          />
        </ScrollView>
      </LinearHeader>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  hzLine: {
    height: 1,
    backgroundColor: '#FFFFFF4d',
    marginBottom: 20,
  },
  viewProfile: {
    flexDirection: 'row',
    marginTop: 30,
    paddingVertical: 30,
    alignItems: 'center',
    justifyContent: 'center',
  },
  imageProfile: {
    width: 90,
    height: 90,
    borderRadius: 45,
    borderWidth: 2,
    borderColor: '#FFFFFF',
  },
  viewDetail: {
    marginLeft: 20,
  },
  textId: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    fontSize: 20,
    color: '#FFFFFF',
    marginBottom: 5,
  },
  textNumber: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 18,
    color: '#FFFFFF',
  },
  touchOption: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 20,
    paddingHorizontal: 25,
  },
  textOption: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    fontSize: 17,
    color: '#FFFFFF',
    marginLeft: 15,
  },
  imageOption: {
    width: 25,
    height: 25,
    resizeMode: 'contain',
  },
});
export default CustomDrawerContent;

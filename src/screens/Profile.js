import React from 'react';
import {
  Image,
  ImageBackground,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
} from 'react-native';
import {StatusBarDark, StatusBarLight} from '../utils/CustomStatusBar';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

const Profile = ({navigation, route}) => {
  return (
    <KeyboardAwareScrollView>
      <View style={styles.container}>
        <StatusBarDark />
        <ImageBackground
          style={styles.ibStyle}
          source={require('../images/p-bg-1.png')}>
          <View style={styles.vStyle}>
            <TouchableOpacity onPress={() => navigation.navigate('Home')}>
              <Image
                source={require('../images/p-back.png')}
                style={styles.backStyle}
              />
            </TouchableOpacity>
            <Image
              source={require('../images/Avatar.png')}
              style={styles.boyStyle}
            />
            <Text style={styles.nameStyle}>Pradeep Yadav</Text>
            <Text style={styles.textStyle}>12th Grade - Commerce</Text>
            <Text style={styles.textStyle}>Petabyte2.5 ID : 9876543212</Text>
            <View style={styles.lineStyle} />
            <Text style={styles.profileStyle}>Profile Details</Text>

            <View style={styles.fieldStyle}>
              <Image
                source={require('../images/name.png')}
                style={styles.nameiStyle}
              />
              <Text style={styles.text1Style}>Pradeep</Text>
            </View>
            <View style={styles.line1Style} />

            <View style={styles.fieldStyle}>
              <Image
                source={require('../images/email.png')}
                style={styles.nameiStyle}
              />
              <Text style={styles.text1Style}>pradeep@gmail.com</Text>
            </View>
            <View style={styles.line1Style} />

            <View style={styles.fieldStyle}>
              <Image
                source={require('../images/mobile.png')}
                style={styles.nameiStyle}
              />
              <Text style={styles.text1Style}>9898989898</Text>
            </View>
            <View style={styles.line1Style} />

            <View style={styles.fieldStyle}>
              <Image
                source={require('../images/gender.png')}
                style={styles.nameiStyle}
              />
              <Text style={styles.text1Style}>Male</Text>
            </View>
            <View style={styles.line1Style} />

            <View style={styles.fieldStyle}>
              <Image
                source={require('../images/dob.png')}
                style={styles.nameiStyle}
              />
              <Text style={styles.text1Style}>10/10/2001</Text>
            </View>
            <View style={styles.line1Style} />

            <View style={styles.fieldStyle}>
              <Image
                source={require('../images/name.png')}
                style={styles.nameiStyle}
              />
              <Text style={styles.text1Style}>Mr. AK</Text>
            </View>
            <View style={styles.line1Style} />

            <View style={styles.fieldStyle}>
              <Image
                source={require('../images/location.png')}
                style={styles.nameiStyle}
              />
              <Text style={styles.text1Style}>New Delhi</Text>
            </View>
            <View style={styles.line1Style} />

            <View style={styles.fieldStyle}>
              <Image
                source={require('../images/city.png')}
                style={styles.nameiStyle}
              />
              <Text style={styles.text1Style}>New Delhi</Text>
            </View>
            <View style={styles.line1Style} />
          </View>
        </ImageBackground>
      </View>
    </KeyboardAwareScrollView>
  );
};

export default Profile;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    paddingBottom: 20,
  },

  ibStyle: {
    flex: 1,
  },
  backStyle: {
    width: 40,
    height: 40,
    resizeMode: 'contain',
    marginTop: 40,
    padding: 10,
  },
  vStyle: {
    width: '90%',
    alignSelf: 'center',
  },
  boyStyle: {
    width: 90,
    height: 90,
    resizeMode: 'contain',
    marginTop: 40,
  },
  nameStyle: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 20,
    color: '#F3414B',
    fontWeight: 'bold',
    marginTop: 30,
  },
  textStyle: {
    fontFamily: 'Avenir-Medium',
    fontSize: 16,
    color: '#1E2432',
    opacity: 0.7,
    fontWeight: '500',
    marginTop: 10,
  },
  lineStyle: {
    width: '100%',
    borderWidth: 1,
    borderColor: '#EAECEF',
    alignSelf: 'center',
    marginTop: 10,
  },
  profileStyle: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 18,
    color: '#1E2432',
    fontWeight: 'bold',
    marginTop: 20,
  },
  nameiStyle: {
    width: 40,
    height: 40,
    resizeMode: 'contain',
  },
  fieldStyle: {
    flexDirection: 'row',

    marginTop: 30,
  },
  text1Style: {
    fontFamily: 'Avenir-Medium',
    fontSize: 18,
    color: '#1E2432',
    opacity: 0.7,
    fontWeight: '500',
    marginLeft: 20,
  },
  line1Style: {
    width: '80%',
    borderWidth: 1,
    borderColor: '#EAECEF',
    alignSelf: 'center',
    marginTop: 0,
    marginLeft: 50,
  },
});

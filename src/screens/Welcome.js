import React, {useEffect} from 'react';
import {
  View,
  Text,
  Dimensions,
  StyleSheet,
  ImageBackground,
  Image,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import {StatusBarLight} from '../utils/CustomStatusBar';

const {width, height} = Dimensions.get('window');

const nuWelcome = ({navigation, route}) => {
  return (
    <View style={styles.vStyle}>
      <StatusBarLight />
      {/* <Text>hrllo</Text> */}

      <ImageBackground
        source={require('../images/bg.png')}
        style={styles.ibStyles}>
        <ScrollView>
          <Image
            source={require('../images/logo-2.png')}
            style={styles.iStyles}
          />
          <Text style={styles.tStyles}>Welcome to Petabyte 2.5</Text>

          <TouchableOpacity
            onPress={() => navigation.navigate('NULogin', {...route.params})}
            style={styles.v1Style}>
            <Text style={styles.t1Styles}>Login</Text>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => navigation.navigate('CreateAccount')}
            style={styles.v2Style}>
            <Text style={styles.t2Styles}>Sign up</Text>
          </TouchableOpacity>
        </ScrollView>
      </ImageBackground>
    </View>
  );
};

export default nuWelcome;

const styles = StyleSheet.create({
  vStyle: {
    flex: 1,
    backgroundColor: 'pink',
  },
  ibStyles: {
    flex: 1,
  },
  iStyles: {
    width: 183,
    height: 168,
    resizeMode: 'contain',
    alignSelf: 'center',
    marginTop: 180,
  },
  tStyles: {
    fontFamily: 'Avenir',
    fontSize: 25,
    fontWeight: '500',
    color: '#FFFFFF',
    alignSelf: 'center',
    marginTop: 15,
  },

  v1Style: {
    backgroundColor: '#FFFFFF',
    width: '90%',
    alignSelf: 'center',
    borderRadius: 25,
    padding: 10,
    marginTop: 200,
  },

  t1Styles: {
    fontFamily: 'Avenir',
    fontSize: 20,
    fontWeight: '500',
    color: '#EF2B75',
    justifyContent: 'center',
    alignSelf: 'center',
    //marginTop: 15,
  },
  v2Style: {
    //backgroundColor: '#FFFFFF',
    width: '90%',
    alignSelf: 'center',
    borderRadius: 25,
    padding: 10,
    marginTop: 20,
    borderColor: '#FFFFFF',
    borderWidth: 1,
  },
  t2Styles: {
    fontFamily: 'Avenir',
    fontSize: 20,
    fontWeight: '500',
    color: '#FFFFFF',
    justifyContent: 'center',
    alignSelf: 'center',
    //marginTop: 15,
  },
});

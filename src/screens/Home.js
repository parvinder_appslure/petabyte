import React, {useEffect, useState} from 'react';
import {
  Image,
  ImageBackground,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  FlatList,
  Dimensions,
  ScrollView,
} from 'react-native';
const {width, height} = Dimensions.get('window');
import {useSelector} from 'react-redux';
import {HomeStudentApi} from '../services/Api';
import {PriceFormat} from '../utils/CommonFunction';
import {StatusBarLight} from '../utils/CustomStatusBar';
import {MainView, MainHeader, RatingView} from '../utils/Header';

const Home = ({navigation, route}) => {
  const {userDetail, userType} = useSelector(state => state);
  const [state, setState] = useState({
    active_courses: [],
    resume_where_you_left: [],
    suggested_course: [],
    userDetail,
    userType,
  });

  useEffect(() => {
    (async () => {
      const {user_id} = state.userDetail;
      if (state.userType === 'student') {
        const response = await HomeStudentApi({
          user_id,
          deviceID: '121212',
          device_token: '21212',
          device_type: '212',
        });
        consolejson(response);
        const {
          status = false,
          user_detail,
          active_courses,
          resume_where_you_left,
          suggested_course,
        } = response;
        if (status) {
          setState({
            ...state,
            user_detail,
            active_courses,
            resume_where_you_left,
            suggested_course,
          });
        } else {
          console.log('student home api failed');
        }
      }
    })();
    return () => {
      console.log('unmount home');
    };
  }, []);
  const Header = () => (
    <MainHeader>
      <View style={styles.headerContainer}>
        <TouchableOpacity onPress={navigation.openDrawer}>
          <Image
            source={require('../assets/menu.png')}
            style={styles.hamburgerIcon}
          />
        </TouchableOpacity>
        <Image source={require('../assets/logo.png')} style={styles.logoIcon} />
        <TouchableOpacity
          style={{marginLeft: 'auto', marginRight: 20}}
          onPress={() => navigation.navigate('Notification')}>
          <Image
            source={require('../assets/notification.png')}
            style={styles.hamburgerIcon}
          />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate('Profile')}>
          <Image
            source={require('../assets/user.png')}
            style={styles.hamburgerIcon}
          />
        </TouchableOpacity>
      </View>
      <View style={{paddingTop: 30, paddingHorizontal: 25}}>
        <Text style={styles.textHello}>Hello !</Text>
        <Text style={styles.textName}>{state.userDetail.name}</Text>
      </View>
    </MainHeader>
  );
  const ActiveCourse = () => {
    const {active_courses} = state;
    return (
      active_courses.length !== 0 && (
        <>
          <OptionTitle title={'Active Course'} />
          <FlatList
            data={active_courses}
            keyExtractor={item => item.id.toString()}
            showsHorizontalScrollIndicator={false}
            horizontal
            renderItem={({item}) => (
              <ImageBackground
                source={require('../images/sub-2.png')}
                style={styles.bgActive}>
                <Image
                  source={require('../images/name.png')}
                  style={styles.imageActive}
                />
                <Text style={styles.textTitleActive}>{item.course_name}</Text>
                <TouchableOpacity
                  style={styles.touchActive}
                  onPress={() => {
                    navigation.navigate('CourseSubject', {
                      user_id: state.userDetail.user_id,
                      course_id: item.course_id,
                    });
                  }}>
                  <Image
                    source={require('../images/Fw.png')}
                    style={styles.imageActiveButton}
                  />
                </TouchableOpacity>
              </ImageBackground>
            )}
          />
        </>
      )
    );
  };
  const Resume = () => {
    const {resume_where_you_left} = state;
    return (
      resume_where_you_left.length !== 0 && (
        <>
          <OptionTitle title={'Resume where you left'} />
        </>
      )
    );
  };
  const SuggestedCourse = () => {
    const {suggested_course} = state;
    return (
      suggested_course.length !== 0 && (
        <>
          <OptionTitle title={'Suggested Courses for you'} />
          <FlatList
            data={state.suggested_course}
            keyExtractor={item => item.id.toString()}
            horizontal={true}
            renderItem={({item}) => (
              <TouchableOpacity
                style={styles.optionTouch}
                onPress={() => {
                  navigation.navigate('SuggestedCourseDetails', item);
                }}>
                <Image
                  source={require('../images/suggested.png')}
                  style={styles.optionImage}
                />
                <Text style={styles.optionTextName}>{item.course_name}</Text>
                <RatingView defaultRating={item.ratings} />
                <OptionPrice
                  price={item.price}
                  percentage={item.discount_per}
                />
              </TouchableOpacity>
            )}
          />
        </>
      )
    );
  };
  return (
    <MainView>
      <StatusBarLight />
      <Header />
      <ScrollView>
        <ActiveCourse />
        <Resume />
        <SuggestedCourse />
      </ScrollView>
    </MainView>
  );
};

export default Home;
const styles = StyleSheet.create({
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  hamburgerIcon: {
    width: 44,
    height: 44,
  },
  logoIcon: {
    width: 50,
    height: 50,
    marginHorizontal: 20,
  },
  textHello: {
    color: 'white',
    fontFamily: 'Avenir-Roman',
    fontWeight: '400',
    fontSize: 24,
    marginBottom: 10,
  },
  textName: {
    color: 'white',
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 30,
  },
  optionTitleText: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    fontSize: 20,
    color: '#1E2432',
    margin: 20,
  },
  optionTouch: {
    width: width / 2 - 20,
    margin: 10,
  },
  optionImage: {
    width: '100%',
    height: 100,
    borderRadius: 10,
  },
  optionTextName: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 18,
    color: '#1E2432',
    paddingVertical: 10,
  },
  optionTextPrice: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    fontSize: 20,
    color: '#FF2D55',
  },
  optionTextDiscount: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 16,
    color: '#C7C7CC',
    textDecorationLine: 'line-through',
  },
  optionViewPrice: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    margin: 5,
  },
  bgActive: {
    height: 120,
    width: width - 40,
    marginHorizontal: 20,
    borderRadius: 10,
    overflow: 'hidden',
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 20,
  },
  textTitleActive: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    fontSize: 18,
    color: '#FFFFFF',
    flex: 1,
  },
  imageActive: {
    width: 70,
    height: 70,
    borderRadius: 35,
    marginRight: 15,
  },
  touchActive: {
    padding: 5,
    marginLeft: 'auto',
  },
  imageActiveButton: {
    width: 30,
    height: 30,
    borderRadius: 15,
    resizeMode: 'contain',
  },
});

const OptionTitle = props => (
  <Text style={styles.optionTitleText}>{props.title}</Text>
);
const OptionPrice = ({price, percentage}) => {
  let bol = +percentage === 0;
  let discount = price - (price * percentage) / 100;

  return (
    <View style={styles.optionViewPrice}>
      <Text style={styles.optionTextPrice}>
        {PriceFormat(bol ? price : discount)}
      </Text>
      {!bol && (
        <Text style={styles.optionTextDiscount}>{PriceFormat(price)}</Text>
      )}
    </View>
  );
};

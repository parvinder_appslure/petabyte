import React from 'react';
import {SafeAreaView, StyleSheet} from 'react-native';
import {SimpleHeader} from '../utils/Header';

const ExamDate = ({navigation, route}) => {
  return (
    <SafeAreaView style={styles.container}>
      <SimpleHeader title={'Exam Dates'} onPress={navigation.goBack} />
    </SafeAreaView>
  );
};

export default ExamDate;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

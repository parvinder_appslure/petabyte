import React, {useState} from 'react';
import {
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import {StatusBarLight} from '../utils/CustomStatusBar';
import {MainView, SimpleHeader, LinearViewTwo} from '../utils/Header';

const SelectStudy = ({navigation, route}) => {
  const OptionView = ({item, onPress}) => (
    <TouchableOpacity onPress={onPress}>
      <LinearViewTwo colors={item.colors} style={styles.linearStyle}>
        <Image source={item.source} style={styles.imageIcon} />
        <Text style={styles.textTitle}>{item.title}</Text>
        <Image
          source={require('../images/p-back1.png')}
          style={styles.imageBack}
        />
      </LinearViewTwo>
    </TouchableOpacity>
  );
  const onStudyHandler = () => console.log('object');
  const onRevisionHandler = () => console.log('object');
  const onMemorizeHandler = () => console.log('object');
  const onAnalyzeHandler = () => console.log('object');
  return (
    <MainView>
      <StatusBarLight />
      <SimpleHeader title={'Select'} onPress={navigation.goBack} />
      <ScrollView style={{paddingVertical: 10}}>
        <OptionView item={optionData.study} onPress={onStudyHandler} />
        <OptionView item={optionData.revision} onPress={onRevisionHandler} />
        <OptionView item={optionData.memorize} onPress={onMemorizeHandler} />
        <OptionView item={optionData.analyze} onPress={onAnalyzeHandler} />
      </ScrollView>
    </MainView>
  );
};

const styles = StyleSheet.create({
  imageIcon: {
    width: 50,
    height: 50,
    resizeMode: 'contain',
    marginRight: 30,
  },
  textTitle: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 18,
    fontWeight: 'bold',
    color: '#FFFFFF',
  },
  imageBack: {
    width: 40,
    height: 40,
    resizeMode: 'contain',
    marginLeft: 'auto',
  },
  linearStyle: {
    marginVertical: 10,
    marginHorizontal: 20,
    padding: 30,
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 15,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
});
const optionData = {
  study: {
    source: require('../images/study.png'),
    title: 'Study',
    colors: ['#F54B3A', '#FF7B02'],
  },
  revision: {
    source: require('../images/revision2.png'),
    title: 'Rivision',
    colors: ['#ED5565', '#D62739'],
  },
  memorize: {
    source: require('../images/memorise2.png'),
    title: 'Memorize',
    colors: ['#2AFEB7', '#08C792'],
  },
  analyze: {
    source: require('../images/analyze.png'),
    title: 'Analyze',
    colors: ['#FFE324', '#FFB533'],
  },
};

export default SelectStudy;

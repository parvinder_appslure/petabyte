import React from 'react';
import {
  Image,
  ImageBackground,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {StatusBarLight} from '../utils/CustomStatusBar';
import {SimpleHeader} from '../utils/Header';
const Subscriptions = ({navigation, route}) => {
  return (
    <ScrollView>
      <SafeAreaView style={styles.container}>
        <SimpleHeader title={'My Subscriptions'} onPress={navigation.goBack} />
        <View style={styles.vStyle}>
          <StatusBarLight />

          <ImageBackground
            style={styles.ibStyle}
            source={require('../images/sub-1.png')}>
            <View style={styles.vStyle}>
              <Text style={styles.tStyle}>1 Month Subscription</Text>

              <View style={styles.v1Style}>
                <Text style={styles.t1Style}>₹ 20050</Text>
                <Image
                  style={styles.iStyle}
                  source={require('../images/check-circle.png')}
                />
              </View>
              <Text style={styles.tStyle}>24 Days Left</Text>
            </View>
          </ImageBackground>
          <Text style={styles.t2Style}>Special offer for you</Text>

          <TouchableOpacity
            onPress={() => navigation.navigate('SchoolSubscriptionsDetails')}>
            <ImageBackground
              style={styles.ibStyle}
              source={require('../images/sub-2.png')}>
              <View style={styles.vStyle}>
                <Text style={styles.tStyle}>3 Month Subscription</Text>

                <View style={styles.v2Style}>
                  <Text style={styles.t1Style}>₹ 5000/-</Text>
                  <Text style={styles.t3Style}>₹ 6000</Text>
                </View>
                <Text style={styles.t3Style}>(10% off)</Text>
              </View>
            </ImageBackground>
          </TouchableOpacity>
          <ImageBackground
            style={styles.ibStyle}
            source={require('../images/sub-3.png')}>
            <View style={styles.vStyle}>
              <Text style={styles.tStyle}>6 Month Subscription</Text>

              <View style={styles.v2Style}>
                <Text style={styles.t1Style}>₹ 9500/-</Text>
                <Text style={styles.t3Style}>₹ 12000</Text>
              </View>
              <Text style={styles.t3Style}>(15% off)</Text>
            </View>
          </ImageBackground>
        </View>
      </SafeAreaView>
    </ScrollView>
  );
};

export default Subscriptions;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  ibStyle: {
    width: '100%',
    //height: 300,
    marginTop: 25,
    resizeMode: 'contain',
    paddingBottom: 20,
    paddingLeft: 5,
    paddingTop: 10,
  },
  vStyle: {
    width: '90%',
    alignSelf: 'center',
  },
  tStyle: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 20,
    fontWeight: 'bold',
    color: '#FFFFFF',
    marginTop: 10,
  },
  t1Style: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 30,
    fontWeight: 'bold',
    color: '#FFFFFF',
  },
  v1Style: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '95%',
    marginTop: 10,
  },
  iStyle: {
    width: 30,
    height: 30,
    resizeMode: 'contain',
  },
  t2Style: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 25,
    fontWeight: 'bold',
    color: '#1E2432',
    marginTop: 20,
  },
  v2Style: {
    flexDirection: 'row',
    width: '95%',
    marginTop: 10,
  },
  t3Style: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 18,
    fontWeight: 'bold',
    color: '#FFFFFF',
    marginTop: 10,
  },
});

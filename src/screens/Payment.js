import React from 'react';
import {View, Text, StyleSheet, Image} from 'react-native';
import {StatusBarLight} from '../utils/CustomStatusBar';

const Payment = () => {
  return (
    <View style={styles.vStyle}>
      <StatusBarLight />
      <View style={styles.tvStyle}>
        <Image
          source={require('../images/gridicons-cross-small.png')}
          style={styles.iStyle}
        />

        {/* <Text>hellow1</Text> */}
      </View>
    </View>
  );
};

export default Payment;

const styles = StyleSheet.create({
  vStyle: {
    backgroundColor: '#FFFFFF',
    flex: 1,
  },
  tvStyle: {
    backgroundColor: '#F13268',
    width: '100%',
    padding: 10,
  },
  iStyle: {
    width: 24,
    height: 24,
    resizeMode: 'contain',
    justifyContent: 'flex-end',
  },
});

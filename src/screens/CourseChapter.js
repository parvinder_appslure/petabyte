import React, {useEffect, useState} from 'react';
import {
  TouchableOpacity,
  Text,
  StyleSheet,
  Image,
  View,
  FlatList,
} from 'react-native';
import {CourseChapterApi} from '../services/Api';
import {StatusBarLight} from '../utils/CustomStatusBar';
import {MainView, SimpleHeader} from '../utils/Header';
import Loader from '../utils/Loader';

const CourseChapter = ({navigation, route}) => {
  const [state, setState] = useState({
    list: [],
    loading: true,
  });
  const OptionView = ({source, title, borderStyle = {}}) => (
    <View style={[styles.ft_touchOption, borderStyle]}>
      <Image source={source} style={styles.ft_imageIcon} />
      <Text style={styles.ft_textOption}>{title}</Text>
    </View>
  );
  useEffect(() => {
    (async () => {
      consolejson(route.params);
      const response = await CourseChapterApi(route.params);
      const {status = false, list = []} = response;
      !status && alert('Something went wrong');
      setState({...state, list, loading: false});
    })();
  }, []);

  return (
    <MainView>
      <StatusBarLight />
      <Loader status={state.loading} />
      <SimpleHeader title={'Select Chapter'} onPress={navigation.goBack} />
      <FlatList
        contentContainerStyle={{paddingVertical: 10}}
        data={state.list}
        keyExtractor={item => item.id.toString()}
        renderItem={({item, index}) => {
          const {subject_id, chapter_name} = item;
          return (
            <View key={`id_${subject_id}`} style={styles.ft_container}>
              <Text style={styles.ft_textChapter}>{`Chapter ${++index}`}</Text>
              <TouchableOpacity
                onPress={() => {
                  navigation.navigate('SelectStudy');
                }}>
                <Text style={styles.ft_textName}>{chapter_name}</Text>
              </TouchableOpacity>
              <View style={styles.ft_viewBottom}>
                <OptionView
                  title={'OverView'}
                  source={require('../images/terms.png')}
                  borderStyle={styles.borderStyle}
                />
                <OptionView
                  title={'Notes'}
                  source={require('../images/terms.png')}
                  borderStyle={styles.borderStyle}
                />
                <OptionView
                  title={'3.0'}
                  source={require('../images/terms.png')}
                  borderStyle={styles.borderStyle}
                />
                <OptionView
                  title={'Level'}
                  source={require('../images/terms.png')}
                />
              </View>
            </View>
          );
        }}
      />
    </MainView>
  );
};

export default CourseChapter;

const styles = StyleSheet.create({
  ft_container: {
    marginHorizontal: 20,
    marginVertical: 10,
    paddingVertical: 20,
    backgroundColor: 'white',
    borderRadius: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  ft_textChapter: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 14,
    color: '#00000080',
    marginHorizontal: 20,
  },
  ft_textName: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 18,
    color: '#1E2432',
    marginVertical: 10,
    marginHorizontal: 20,
  },
  ft_viewBottom: {
    flexDirection: 'row',
  },
  ft_touchOption: {
    flex: 0.25,
    alignItems: 'center',
  },
  ft_imageIcon: {
    height: 30,
    width: 30,
    resizeMode: 'contain',
    borderRadius: 18,
    marginVertical: 10,
  },
  ft_textOption: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 14,
    color: '#00000080',
    marginBottom: 5,
  },
  borderStyle: {
    borderRightWidth: 1,
    borderRightColor: '#0000004d',
  },
});

import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  Dimensions,
  StyleSheet,
  ScrollView,
  TextInput,
} from 'react-native';
import {StatusBarLight} from '../utils/CustomStatusBar';
import {globStyle} from '../style/style';
import {
  AsyncStorageSetUserId,
  SignInUserApi,
  SignInStudentApi,
} from '../services/Api';
import * as actions from '../redux/actions';
import {useDispatch} from 'react-redux';
import {
  BackButton,
  BackgroundImage,
  ForgotPasswordButton,
  FormView,
  H1,
  H2,
  MainView,
  SubmitButton,
} from '../utils/Header';

const {width, height} = Dimensions.get('window');

const Login = ({navigation, route}) => {
  const dispatch = useDispatch();
  const [state, setState] = useState({
    uid: '',
    mobile: '',
    password: '',
  });
  const [loader, setLoader] = useState(false);
  useEffect(() => {
    console.log('Id', state);
    console.log('password', state);
    consolejson(route.params);
  }, []);

  const onLoginHandler = async () => {
    const {userType} = route.params;
    const {uid, mobile, password} = state;
    if (password === '') {
      alert('Please enter your password');
      return;
    }
    if (userType === 'individual') {
      if (mobile === '') {
        alert('Please enter your mobile');
        return;
      }
      const body = {
        mobile: '8290838118',
        password: '123456',
        deviceID: 'sda',
        device_type: 'asd',
        device_token: 'sad',
      };
      const response = await SignInUserApi(body);
      responseHandler(response, userType);
    }
    if (userType === 'student') {
      if (uid === '') {
        alert('Please enter your Acecodes Id');
        return;
      }
      const body = {
        uid: '123123',
        password: '123456',
        deviceID: 'sda',
        device_type: 'asd',
        device_token: 'sad',
      };
      const response = await SignInStudentApi(body);
      responseHandler(response, userType);
    }
  };

  const responseHandler = (response, userType) => {
    consolejson(response);
    const {status = false, user_detail = {}} = response;
    if (status) {
      dispatch(actions.userDetail({userDetail: user_detail, userType}));
      AsyncStorageSetUserId({userId: user_detail.user_id, userType});
      navigation.reset({
        index: 0,
        routes: [{name: 'DrawerNavigator'}],
      });
    } else {
      alert('error');
    }
  };
  return (
    <MainView>
      <StatusBarLight />
      <BackgroundImage>
        <ScrollView>
          <BackButton onPress={navigation.goBack} />
          <H1 text={'Hi, Welcome back!'} />
          <H2 text={'Please Log-in to continue'} />
          <View style={styles.viewForm}>
            <View style={styles.viewFormbg} />
            <FormView containerStyle={{marginTop: 0}}>
              {route.params.userType === 'student' && (
                <TextInput
                  placeholder="Acecodes ID"
                  underlineColorAndroid="#EAECEF"
                  value={state.uid}
                  onChangeText={uid => setState({...state, uid})}
                  style={globStyle.textInput}
                />
              )}
              {route.params.userType === 'individual' && (
                <TextInput
                  placeholder="Mobile"
                  underlineColorAndroid="#EAECEF"
                  value={state.mobile}
                  keyboardType={'number-pad'}
                  onChangeText={mobile => setState({...state, mobile})}
                  style={globStyle.textInput}
                />
              )}

              <TextInput
                placeholder="Password"
                underlineColorAndroid="#EAECEF"
                value={state.password}
                keyboardType={'default'}
                secureTextEntry={true}
                onChangeText={password => setState({...state, password})}
                style={globStyle.textInput}
              />
              <ForgotPasswordButton
                onPress={() =>
                  navigation.navigate('ForgetPassword', {...route.params})
                }
              />
              <SubmitButton onPress={onLoginHandler} title={'Login'} />
            </FormView>
          </View>
        </ScrollView>
      </BackgroundImage>
    </MainView>
  );
};

export default Login;

const styles = StyleSheet.create({
  viewForm: {
    marginTop: 30,
    paddingBottom: 35,
  },
  viewFormbg: {
    position: 'absolute',
    height: '100%',
    width: '85%',
    right: 0,
    bottom: 0,
    backgroundColor: 'white',
    borderBottomLeftRadius: 15,
  },
});

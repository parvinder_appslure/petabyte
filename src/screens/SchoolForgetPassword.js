import React, {useEffect, useState} from 'react';

import {
  View,
  Text,
  Dimensions,
  StyleSheet,
  ImageBackground,
  Image,
  TouchableOpacity,
  ScrollView,
  TextInput,
  Modal,
} from 'react-native';

import {StatusBarLight} from '../utils/CustomStatusBar';

const {width, height} = Dimensions.get('window');

const SchoolForgetPassword = ({navigation}) => {
  const [modalOpen, setModalOpen] = useState(false);

  const [state, setState] = useState({
    Id: '',
  });
  useEffect(() => {
    console.log('Id', state);
  });

  return (
    <View style={styles.i_Style}>
      <StatusBarLight />
      <ImageBackground
        source={require('../images/bg.png')}
        style={styles.i_Style}>
        <ScrollView>
          <Image
            source={require('../images/back.png')}
            style={styles.i2_Style}
          />
          <View style={styles.v_Style}>
            <Text style={styles.t_Style}>Forgot password</Text>
            <Text style={styles.t2_Style}>Please enter your details.</Text>

            <View
              style={{
                backgroundColor: 'white',
                width: '95%',
                borderRadius: 10,
                alignSelf: 'center',
                marginTop: 50,
                paddingBottom: 30,
                marginLeft: 25,
              }}>
              <TextInput
                placeholder="Email/MobileSC"
                underlineColorAndroid="#EAECEF"
                value={state.Id}
                onChangeText={text => setState({...setState, Id: text})}
                style={styles.ipStyle}
              />
              {/* <TextInput
                placeholder="Email/Mobile"
                underlineColorAndroid="#EAECEF"
                value={state.Id}
                onChangeText={text => setState({...setState, Id: text})}
                style={styles.ipStyle}
              />

<TextInput
                placeholder="Email/Mobile"
                underlineColorAndroid="#EAECEF"
                value={state.Id}
                onChangeText={text => setState({...setState, Id: text})}
                style={styles.ipStyle}
              />

<TextInput
                placeholder="Email/Mobile"
                underlineColorAndroid="#EAECEF"
                value={state.Id}
                onChangeText={text => setState({...setState, Id: text})}
                style={styles.ipStyle}
              />
              <TextInput
                placeholder="Email/Mobile"
                underlineColorAndroid="#EAECEF"
                value={state.Id}
                onChangeText={text => setState({...setState, Id: text})}
                style={styles.ipStyle}
              /> */}

              {/* <TouchableOpacity
                onPress={() => {
                  setModalOpen(false);
                  navigation.navigate('CreatePassword');
                }}>
                <Modal visible={modalOpen} transparent={true}>
                  <View style={styles.mvStyle}>
                    <View style={styles.mv1Style}>
                      <Image
                        source={require('../images/check-green.png')}
                        style={styles.mi_Style}
                      />
                      <View style={styles.v_Style}>
                        <Text style={styles.mt_Style}>Hi</Text>
                        <Text style={styles.tb3_Style}>
                          {`Hi, We have successfully verified your details.You Can Change your password at next screen.`}
                        </Text>
                      </View>
                    </View>
                  </View>
                </Modal>
              </TouchableOpacity> */}

              <TouchableOpacity
                // onPress={() => setModalOpen(true)}
                onPress={() => navigation.navigate('CreatePassword')}
                style={{
                  backgroundColor: '#F13268',
                  borderColor: 20,
                  width: '90%',
                  alignSelf: 'center',
                  borderRadius: 25,
                  padding: 15,
                  marginTop: 50,
                }}>
                <Text style={styles.tb2_Style}>Submit</Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </ImageBackground>
    </View>
  );
};

export default SchoolForgetPassword;

const styles = StyleSheet.create({
  i_Style: {
    flex: 1,
  },
  v_Style: {
    width: '90%',
    alignSelf: 'center',
  },
  i2_Style: {
    width: 40,
    height: 40,
    resizeMode: 'contain',
    marginTop: 40,
    marginLeft: 30,
  },
  t_Style: {
    fontFamily: 'Avenir',
    fontSize: 34,
    fontWeight: 'bold',
    color: '#FFFFFF',
    marginTop: 30,
    alignSelf: 'center',
    marginLeft: -30,
  },
  t2_Style: {
    fontFamily: 'Avenir',
    fontSize: 17,
    fontWeight: '500',
    color: '#FFFFFF',
    marginLeft: 22,
    marginTop: 10,
  },

  tb2_Style: {
    fontFamily: 'Avenir',
    fontSize: 17,
    fontWeight: 'bold',
    color: '#FFFFFF',
    alignSelf: 'center',
    justifyContent: 'center',
  },
  ipStyle: {
    marginTop: 20,
    width: '95%',
    alignSelf: 'flex-end',
    color: '#333A4D',
    fontSize: 17,
  },

  mvStyle: {
    backgroundColor: '#000000aa',
    flex: 1,
  },
  mv1Style: {
    width: '90%',
    //height: '50%',
    alignSelf: 'center',
    backgroundColor: '#FFFFFF',
    marginTop: 150,
    borderRadius: 20,
    padding: 10,
  },
  mi_Style: {
    width: 100,
    height: 100,
    resizeMode: 'contain',
    alignSelf: 'center',
    marginTop: 30,
  },
  mt_Style: {
    fontFamily: 'Avenir',
    fontSize: 34,
    fontWeight: '500',
    marginTop: 15,
    alignSelf: 'center',
  },
  tb3_Style: {
    fontFamily: 'Avenir',
    fontSize: 17,
    fontWeight: '500',
    marginTop: 10,
    alignSelf: 'center',
    color: '#838383',
  },
});

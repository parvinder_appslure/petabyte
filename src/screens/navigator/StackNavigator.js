import React, {useRef} from 'react';
import {
  CardStyleInterpolators,
  createStackNavigator,
} from '@react-navigation/stack';
import {NavigationContainer} from '@react-navigation/native';
import Splash from '../Splash';
import selectlanguage from '../SelectLanguage';
import schoolUserType from '../SchoolUserType';
import nuWelcome from '../Welcome';
import DrawerNavigator from './DrawerNavigator';
import NULogin from '../Login';
import Otp from '../Otp';
import SuggestedCourseDetails from '../SuggestedCourseDetails';
import Payment from '../Payment';
import ForgetPassword from '../ForgetPassword';
import CreatePassword from '../CreatePassword';
import SelectTime from '../SelectTime';
import SchoolForgetPassword from '../SchoolForgetPassword';
import CreateAccount from '../CreateAccount';
import Notification from '../Notification';
import Profile from '../Profile';
import Settings from '../Settings';
import ExamDate from '../ExamDate';
import Subscriptions from '../Subscriptions';
import SelectBoard from '../SelectBoard';
import SelectCourse from '../SelectCource';
import SubscriptionDetails from '../SubscriptionDetails';
import SchoolSubscriptionsDetails from '../SchoolSubscriptionsDetails';
import SuggestedCoursePurchase from '../SuggestedCoursePurchase';
import CourseSubject from '../CourseSubject';
import CourseChapter from '../CourseChapter';
import SelectStudy from '../SelectStudy';
import Study from '../Study';
import MemorizeSetting from '../MemorizeSetting';
import ChemistryCard from '../ChemistryCard';

const Stack = createStackNavigator();
const StackNavigator = () => {
  const navigationRef = useRef();
  // useReduxDevToolsExtension(navigationRef);
  return (
    <NavigationContainer ref={navigationRef}>
      <Stack.Navigator
        initialRouteName={'Splash'}
        screenOptions={{
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        }}>
        <Stack.Screen
          name="Splash"
          component={Splash}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="DrawerNavigator"
          component={DrawerNavigator}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Notification"
          component={Notification}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Profile"
          component={Profile}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Subscriptions"
          component={Subscriptions}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="ExamDate"
          component={ExamDate}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Settings"
          component={Settings}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="selectLanguage"
          component={selectlanguage}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="SelectTime"
          component={SelectTime}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="schoolUserType"
          component={schoolUserType}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="nuWelcome"
          component={nuWelcome}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="NULogin"
          component={NULogin}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Otp"
          component={Otp}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="CourseSubject"
          component={CourseSubject}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="CourseChapter"
          component={CourseChapter}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="SuggestedCourseDetails"
          component={SuggestedCourseDetails}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="SuggestedCoursePurchase"
          component={SuggestedCoursePurchase}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Payment"
          component={Payment}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="ForgetPassword"
          component={ForgetPassword}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="CreatePassword"
          component={CreatePassword}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="SchoolForgetPassword"
          component={SchoolForgetPassword}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="CreateAccount"
          component={CreateAccount}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="SelectBoard"
          component={SelectBoard}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="SelectCourse"
          component={SelectCourse}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="SubscriptionDetails"
          component={SubscriptionDetails}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="SchoolSubscriptionsDetails"
          component={SchoolSubscriptionsDetails}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="SelectStudy"
          component={SelectStudy}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Study"
          component={Study}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="MemorizeSetting"
          component={MemorizeSetting}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="ChemistryCard"
          component={ChemistryCard}
          options={{headerShown: false}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default StackNavigator;

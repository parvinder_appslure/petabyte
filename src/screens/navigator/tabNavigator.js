import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Home from '../Home';
import Courses from '../Courses';
import MyCards from '../MyCards';
import {Image, StyleSheet} from 'react-native';

const iconPath = {
  ha: require('../../assets/home_a.png'),
  hd: require('../../assets/home_d.png'),
  ca: require('../../assets/course_a.png'),
  cd: require('../../assets/course_d.png'),
  ma: require('../../assets/card_a.png'),
  md: require('../../assets/card_d.png'),
};

const Tab = createBottomTabNavigator();
const TabIcon = source => <Image source={source} style={styles.tabIcon} />;

const TabNavigator = () => {
  return (
    <Tab.Navigator
      initialRouteName="Home"
      tabBarOptions={{
        activeTintColor: '#FF2D55',
        labelStyle: {
          fontFamily: 'Avenir-Medium',
          fontWeight: '500',
          fontSize: 12,
        },
        style: {
          backgroundColor: '#F8F8F8',
        },
      }}>
      <Tab.Screen
        name="Home"
        component={Home}
        options={{
          tabBarIcon: ({focused}) =>
            TabIcon(focused ? iconPath.ha : iconPath.hd),
        }}
      />
      <Tab.Screen
        name="Courses"
        component={Courses}
        options={{
          tabBarIcon: ({focused}) =>
            TabIcon(focused ? iconPath.ca : iconPath.cd),
        }}
      />
      <Tab.Screen
        name="My Cards"
        component={MyCards}
        options={{
          tabBarIcon: ({focused}) =>
            TabIcon(focused ? iconPath.ma : iconPath.md),
        }}
      />
      {/* <Tab.Screen
        name="Home"
        component={}
        options={{
          tabBarIcon: ({focused}) =>
            TabIcon(focused ? iconPath.ha : iconPath.h),
        }}
      /> */}
      {/* <Tab.Screen
        name="StoreScreen"
        component={Store2}
        options={{
          tabBarIcon: ({focused}) =>
            TabIcon(focused ? iconPath.sa : iconPath.s),
          tabBarLabel: 'Store',
        }}
      />
      <Tab.Screen
        name="MyOrder"
        component={Order}
        options={{
          tabBarIcon: ({focused}) =>
            TabIcon(focused ? iconPath.ma : iconPath.m),
          tabBarLabel: 'Upselling',
        }}
      />
      <Tab.Screen
        name="MyProfile"
        component={MyProfile}
        options={{
          tabBarIcon: ({focused}) =>
            TabIcon(focused ? iconPath.pa : iconPath.p),
          tabBarLabel: 'My Profile',
        }}
      /> */}
    </Tab.Navigator>
  );
};
export default TabNavigator;

const styles = StyleSheet.create({
  tabIcon: {
    height: 20,
    width: 20,
    resizeMode: 'contain',
  },
});

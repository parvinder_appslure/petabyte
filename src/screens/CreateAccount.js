import React, {useEffect, useState} from 'react';

import {
  View,
  StyleSheet,
  Image,
  Dimensions,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import {StatusBarLight} from '../utils/CustomStatusBar';
import {Dropdown} from 'react-native-material-dropdown-v2';
import {TextField} from 'react-native-material-textfield';
import {
  BackgroundImage,
  MainView,
  SimpleHeaderTwo,
  SubmitButton,
} from '../utils/Header';
const {width, height} = Dimensions.get('window');

import DateTimePicker from '@react-native-community/datetimepicker';
import moment from 'moment';
import {OtpApi} from '../services/Api';

import {GenerateOtp} from '../utils/CommonFunction';
const dropDownList = [
  {
    id: '1',
    value: 'Male',
  },
  {
    id: '2',
    value: 'Female',
  },
];

const CreateAccount = ({navigation, route}) => {
  const [state, setState] = useState({
    date: '',
    showPicker: false,
    gender: '',
  });
  const FormImage = props => (
    <Image source={props.source} style={styles.imageInput} />
  );
  const onSubmitHandler = async () => {
    console.log('submit handler');
    const otp = GenerateOtp();
    const json = {
      email: 'parvinder77@gmail.com',
      mobile: '888277262477',
      otp,
    };
    const body = {
      name: 'Sachin Applsure',
      email: 'parvinder77@gmail.com',
      mobile: '888277262477',
      deviceID: '121212',
      device_token: '21212',
      device_type: '212',
      password: '123456',
      preferred_timing: '6am-10pm',
      lang: 'en',
      latitude: 'we',
      longitude: '23',
      class_id: '11',
      otp,
    };
    const response = await OtpApi(json);
    const {status = false, msg = 'Something went wrong'} = response;
    if (status) {
      navigation.navigate('Otp', {
        userType: 'individualSignUp',
        body,
      });
    } else {
      alert(msg);
    }
  };

  const showDateTimePicker = () => (
    <DateTimePicker
      value={state.date || new Date()}
      minimumDate={new Date()}
      mode={'date'}
      display="spinner"
      is24Hour={false}
      onChange={({type, nativeEvent}) => {
        if (type === 'set') {
          setState({
            ...state,
            showPicker: false,
            dob: moment(nativeEvent.timestamp).format('YYYY-MM-DD'),
            date: nativeEvent.timestamp,
          });
        } else {
          setState({...state, showPicker: false});
        }
      }}
    />
  );
  return (
    <MainView>
      <StatusBarLight />
      <ScrollView>
        <BackgroundImage style={{height: height / 4}}>
          <SimpleHeaderTwo
            onPress={navigation.goBack}
            title={'Create an account'}
          />
        </BackgroundImage>
        <View style={styles.viewFormContainer}>
          <View style={styles.viewForm}>
            <View style={styles.viewInput}>
              <FormImage source={require('../images/name.png')} />
              <TextField
                label={'Name'}
                defaultValue={state.name}
                onChangeText={name => setState({...state, name})}
                tintColor="grey"
                containerStyle={{flex: 1}}
              />
            </View>

            <View style={styles.viewInput}>
              <FormImage source={require('../images/name.png')} />
              <TextField
                label={'Mobile'}
                defaultValue={state.mobile}
                onChangeText={mobile => setState({...state, mobile})}
                tintColor="grey"
                containerStyle={{flex: 1}}
              />
            </View>

            <View style={styles.viewInput}>
              <FormImage source={require('../images/name.png')} />
              <TextField
                label={'Mobile'}
                defaultValue={state.mobile}
                onChangeText={mobile => setState({...state, mobile})}
                tintColor="grey"
                containerStyle={{flex: 1}}
              />
            </View>
            <View style={styles.viewInput}>
              <FormImage source={require('../images/name.png')} />
              <Dropdown
                rippleOpacity={0.54}
                dropdownPosition={-2}
                baseColor={state.gender ? '#242E42' : '#8F8F8F'}
                // onChangeText={(value, index) => {
                //   setState({...state, airport_id: value});
                // }}
                itemTextStyle={styles.dropDownTextStyle}
                label={'Gender'}
                data={dropDownList}
                labelExtractor={item => item.value}
                valueExtractor={item => item.id}
                containerStyle={{
                  flex: 1,
                  // backgroundColor: 'yellow',
                }}
                pickerStyle={{width: '70%'}}
              />
            </View>

            <View style={styles.viewInput}>
              <FormImage source={require('../images/name.png')} />
              <TextField
                label={'Mobile'}
                defaultValue={state.mobile}
                onChangeText={mobile => setState({...state, mobile})}
                tintColor="grey"
                containerStyle={{flex: 1}}
              />
            </View>
            <View style={styles.viewInput}>
              <FormImage source={require('../images/name.png')} />
              <TextField
                label={'Mobile'}
                defaultValue={state.mobile}
                onChangeText={mobile => setState({...state, mobile})}
                tintColor="grey"
                containerStyle={{flex: 1}}
              />
            </View>
            <View style={[styles.viewInput, {marginBottom: 0}]}>
              <FormImage source={require('../images/name.png')} />
              <TouchableOpacity
                style={{flex: 1}}
                onPress={() => setState({...state, showPicker: true})}>
                <TextField
                  label={'Date of birth'}
                  defaultValue={state.dob}
                  tintColor="grey"
                  editable={false}
                />
              </TouchableOpacity>
            </View>

            <SubmitButton title={'Submit'} onPress={onSubmitHandler} />
          </View>
        </View>

        {state.showPicker && showDateTimePicker()}
      </ScrollView>
    </MainView>
  );
};

export default CreateAccount;
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  ibStyle: {
    width: '100%',
    height: '50%',
  },
  vStyle: {
    flexDirection: 'row',
    alignSelf: 'center',
    marginTop: 40,
    width: '90%',
  },
  viewFormContainer: {
    paddingBottom: 40,
  },
  viewForm: {
    marginHorizontal: 30,
    marginTop: -height / 10,
    backgroundColor: '#FFFFFF',
    borderRadius: 10,
    paddingTop: 20,
    paddingBottom: 30,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  imageInput: {
    width: 40,
    height: 40,
    resizeMode: 'contain',
    marginHorizontal: 20,
  },
  textInput: {
    alignSelf: 'center',
    color: '#333A4D',
    fontSize: 17,
    flex: 1,
  },
  viewInput: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 20,
  },
  t1Style: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: '500',
    fontSize: 20,
    color: '#FFFFFF',
    //marginLeft: 40,
    alignSelf: 'center',
    justifyContent: 'center',
  },
  v3Style: {
    backgroundColor: '#F13268',
    width: '90%',
    padding: 15,
    borderRadius: 25,
    marginTop: 50,
    alignSelf: 'center',
  },

  dropDownTextStyle: {
    fontFamily: 'Avenir-Medium',
    fontSize: 17,
    color: '#242E42',
    backgroundColor: 'red',
  },
});

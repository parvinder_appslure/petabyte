import React, {useEffect, useState} from 'react';

import {
  View,
  Text,
  StyleSheet,
  ImageBackground,
  Image,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import {StatusBarLight} from '../utils/CustomStatusBar';

const SubscriptionDetails = ({navigation}) => {
  return (
    <View style={styles.container}>
      <StatusBarLight />
      <ImageBackground
        source={require('../images/bg.png')}
        style={styles.ibStyle}>
        <View style={styles.vStyle}>
          <Image source={require('../images/back.png')} style={styles.iStyle} />
          <Text style={styles.tStyle}>Subscriptions Details</Text>
        </View>

        <TouchableOpacity
          onPress={() => navigation.navigate('SelectBoard')}
          style={styles.v3Style}>
          <Text style={styles.t1Style}>Pay Now</Text>
        </TouchableOpacity>
      </ImageBackground>
    </View>
  );
};

export default SubscriptionDetails;
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  ibStyle: {
    width: '100%',
    height: '50%',
  },
  vStyle: {
    flexDirection: 'row',
    alignSelf: 'center',
    marginTop: 40,
    width: '90%',
  },
  iStyle: {
    width: 40,
    height: 40,
    resizeMode: 'contain',
  },
  tStyle: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 20,
    color: '#FFFFFF',
    marginLeft: 40,
  },
  v1Style: {
    width: '90%',
    alignSelf: 'center',
    backgroundColor: '#FFFFFF',
    borderRadius: 10,
    paddingBottom: 20,
  },
  i1Style: {
    width: 40,
    height: 40,
    resizeMode: 'contain',
    marginTop: 25,
  },
  ipStyle: {
    marginTop: 20,
    width: '95%',
    alignSelf: 'center',
    color: '#333A4D',
    fontSize: 17,
    marginLeft: 10,
  },
  v2Style: {
    flexDirection: 'row',
    width: '80%',
    //alignSelf: 'center',
    justifyContent: 'space-between',
    marginLeft: 15,
  },
  t1Style: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: '500',
    fontSize: 20,
    color: '#FFFFFF',
    //marginLeft: 40,
    alignSelf: 'center',
    justifyContent: 'center',
  },
  v3Style: {
    backgroundColor: '#F13268',
    width: '90%',
    padding: 15,
    borderRadius: 25,
    marginTop: 50,
    alignSelf: 'center',
  },
});

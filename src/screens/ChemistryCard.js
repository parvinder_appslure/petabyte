import React, {useState} from 'react';
import {
  ImageBackground,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Image,
  ScrollView,
} from 'react-native';
import {StatusBarLight} from '../utils/CustomStatusBar';
import {SimpleHeader} from '../utils/Header';

const ChemistryCard = ({navigation, route}) => {
  return (
    <SafeAreaView style={styles.container}>
      <ScrollView>
        <SimpleHeader title={'Chemistry'} onPress={navigation.goBack} />
        <StatusBarLight />
        <View style={styles.topView}>
          <Text style={styles.txtstyle}>Chapter 1</Text>
          <Text style={styles.txt1style}>Some Basic Concepts Of Chemistry</Text>
          <Text style={styles.txt3style}>1. Importance of Chemistry</Text>
          <View style={styles.viewStyle}>
            <View style={styles.view1Style}>
              <Image
                source={require('../images/card.png')}
                style={styles.cardimg}
              />
              <Text style={styles.imgtext}>Chemistry</Text>
            </View>

            <View style={styles.view3Style}>
              <Image
                source={require('../images/card.png')}
                style={styles.cardimg}
              />
              <Text style={styles.imgtext}>Chemistry</Text>
            </View>
          </View>

          <View style={styles.lineStyle} />

          <Text style={styles.txt3style}>2. Nature of Matter</Text>
          <View style={styles.viewStyle}>
            <View style={styles.view1Style}>
              <Image
                source={require('../images/card.png')}
                style={styles.cardimg}
              />
              <Text style={styles.imgtext}>Chemistry</Text>
            </View>

            <View style={styles.view2Style}>
              <Image
                source={require('../images/card.png')}
                style={styles.cardimg}
              />
              <Text style={styles.imgtext}>Chemistry</Text>
            </View>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default ChemistryCard;

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFFFFF',
    flex: 1,
  },
  txtstyle: {
    fontFamily: 'Avenir-Medium',
    fontSize: 15,
    fontWeight: '500',
    color: '#000000',
    opacity: 0.25,
  },
  topView: {
    width: '90%',
    alignSelf: 'center',
    marginTop: 20,
  },
  txt1style: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 25,
    fontWeight: 'bold',
    color: '#F3414B',
    marginTop: 10,
  },
  txt2style: {
    fontFamily: 'Avenir-Medium',
    fontSize: 15,
    fontWeight: '500',
    color: '#000000',
    opacity: 0.75,
    marginTop: 10,
  },
  txt3style: {
    fontFamily: 'Avenir-Medium',
    fontSize: 18,
    fontWeight: '500',
    color: '#000000',
    marginTop: 30,
  },
  viewStyle: {
    marginTop: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  videoimg: {
    width: 150,
    height: 100,
    resizeMode: 'contain',
  },
  view1Style: {
    backgroundColor: '#EF5350',
    width: '47%',
    padding: 10,
    borderRadius: 15,
  },
  playimg: {
    width: 30,
    height: 30,
    resizeMode: 'contain',
    alignSelf: 'center',
    marginTop: 35,
  },
  imgtext: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    color: '#FFFFFF',
    marginTop: 5,
    marginLeft: 20,
  },
  cardimg: {
    width: 40,
    height: 40,
    resizeMode: 'contain',
    alignSelf: 'center',
    marginTop: 10,
    alignSelf: 'flex-end',
  },
  lineStyle: {
    width: '100%',
    borderWidth: 1,
    borderColor: '#000000',
    opacity: 0.1,
    marginTop: 20,
  },
  view2Style: {
    backgroundColor: '#FFA726',
    width: '47%',
    padding: 15,
    borderRadius: 15,
  },
  view3Style: {
    backgroundColor: '#4CAF50',
    width: '47%',
    padding: 15,
    borderRadius: 15,
  },
});

import React, {useState} from 'react';
import {
  Image,
  ImageBackground,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {StatusBarLight} from '../utils/CustomStatusBar';
import {LinearHeader, SimpleHeader, SubmitButton} from '../utils/Header';
import {Dropdown} from 'react-native-material-dropdown-v2';
const dropDownList = [
  {
    id: '1',
    value: '1 Month',
  },
  {
    id: '2',
    value: '3 Months',
  },
  {
    id: '2',
    value: '6 Months',
  },
  {
    id: '2',
    value: '12 Months',
  },
];

const SchoolSubscriptionsDetails = ({navigation, route}) => {
  const [state, setState] = useState({
    date: '',
    showPicker: false,
    gender: '',
  });

  return (
    <ScrollView>
      <SafeAreaView style={styles.container}>
        <SimpleHeader
          title={'Subscriptions Details'}
          onPress={navigation.goBack}
        />
        <View style={styles.vStyle}>
          <StatusBarLight />
          <Text style={styles.tStyle}>Select Your Subscriptions</Text>
          <Text style={styles.t1Style}>
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry.
          </Text>
          <Text style={styles.t2Style}>Select payment period</Text>

          <Dropdown
            rippleOpacity={0.54}
            dropdownPosition={-2}
            baseColor={state.gender ? '#242E42' : '#8F8F8F'}
            // onChangeText={(value, index) => {
            //   setState({...state, airport_id: value});
            // }}
            itemTextStyle={styles.dropDownTextStyle}
            label={'Gender'}
            data={dropDownList}
            labelExtractor={item => item.value}
            valueExtractor={item => item.id}
            containerStyle={{
              flex: 1,
              // backgroundColor: 'yellow',
            }}
            pickerStyle={{width: '70%'}}
          />

          <Text style={styles.t3Style}>*Lorem Ipsum is simply</Text>
          <Text style={styles.t4Style}>₹ 2000/month</Text>
          <SubmitButton onPress={navigation.navigate('')} title={'Login'} />
        </View>
      </SafeAreaView>
    </ScrollView>
  );
};

export default SchoolSubscriptionsDetails;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  vStyle: {
    width: '90%',
    alignSelf: 'center',
  },
  tStyle: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 30,
    color: '#F33C55',
    alignSelf: 'center',
    lineHeight: 40,
    marginTop: 20,
  },
  t1Style: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 18,
    color: '#8D8D8D',
    alignSelf: 'center',
    marginTop: 20,
  },
  t2Style: {
    fontFamily: 'Avenir-Roman',
    fontSize: 20,
    color: '#292929',
    alignSelf: 'center',
    marginTop: 20,
    fontWeight: '600',
  },
  t3Style: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 14,
    color: '#8D8D8D',
    //alignSelf: 'center',
    marginTop: 20,
  },
  t4Style: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 35,
    fontWeight: 'bold',
    color: '#F33C55',
    alignSelf: 'center',
    lineHeight: 40,
    marginTop: 20,
  },
});

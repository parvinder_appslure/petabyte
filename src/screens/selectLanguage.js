import React from 'react';

import {
  Image,
  ImageBackground,
  Text,
  View,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {StatusBarLight} from '../utils/CustomStatusBar';

const {width, height} = Dimensions.get('window');

const selectlanguage = ({navigation}) => {
  return (
    <>
      <View style={{backgroundColor: 'pink', flex: 1}}>
        <StatusBarLight />
        <ImageBackground
          source={require('../images/bg.png')}
          style={styles.i_Style}>
          <Image
            source={require('../images/back.png')}
            style={styles.i2_Style}
          />
          <View style={styles.v_Style}>
            <Text style={styles.t_Style}>Choose Your Preffered Language</Text>
            <TouchableOpacity
              onPress={() => navigation.navigate('select_Time')}
              style={styles.v1_Style}>
              <View style={styles.v2_Style}>
                <Text style={styles.t2_Style}>English</Text>
                <Image
                  source={require('../images/check-1.png')}
                  style={styles.i3_Style}
                />
              </View>
            </TouchableOpacity>

            <View style={styles.v1_Style}>
              <Text style={styles.t3_Style}>Hindi</Text>
            </View>
          </View>
        </ImageBackground>
      </View>
    </>
  );
};

export default selectlanguage;

const styles = StyleSheet.create({
  i_Style: {
    flex: 1,
  },
  v_Style: {
    width: '90%',
    alignSelf: 'center',
  },
  i2_Style: {
    width: 40,
    height: 40,
    resizeMode: 'contain',
    position: 'absolute',
    marginTop: 40,
    marginLeft: 20,
  },
  t_Style: {
    fontFamily: 'Avenir',
    fontSize: 34,
    fontWeight: 'bold',
    color: '#FFFFFF',
    marginTop: 100,
    alignSelf: 'center',
  },
  v1_Style: {
    backgroundColor: 'white',
    borderRadius: 20,
    width: '94%',
    alignSelf: 'center',
    marginTop: 60,
    padding: 12,
  },
  t2_Style: {
    fontFamily: 'Avenir',
    fontSize: 18,
    fontWeight: '500',
    color: '#FF2D55',
  },
  i3_Style: {
    width: 30,
    height: 30,
    resizeMode: 'contain',
  },
  v2_Style: {
    width: '90%',
    alignSelf: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  t3_Style: {
    fontFamily: 'Avenir',
    fontSize: 18,
    fontWeight: '500',
    color: '#FF2D55',
    marginLeft: 20,
  },
});

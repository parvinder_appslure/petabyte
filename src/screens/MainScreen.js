import React from 'react';
import {SafeAreaView, StyleSheet} from 'react-native';

const MainScreen = ({}) => {
  return <SafeAreaView style={styles.container}></SafeAreaView>;
};

export default MainScreen;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'pink',
  },
});

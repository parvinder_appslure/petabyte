import React, {useEffect, useState} from 'react';
import {
  ImageBackground,
  Image,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  FlatList,
} from 'react-native';
import {LinearHeader} from '../utils/Header';
const MyCards = ({navigation}) => {
  const [people, setPeople] = useState([
    {
      key: 1,
      source: require('../images/che.png'),
      subject: 'Chemistry',
      nav: 'ChemistryCard',
    },
    {
      key: 2,
      source: require('../images/phy.png'),
      subject: 'Physics',
    },
    {
      key: 3,
      source: require('../images/b.png'),
      subject: 'Biology',
    },
    {
      key: 4,
      source: require('../images/m.png'),
      subject: 'Maths',
    },
    {
      key: 5,
      source: require('../images/eng.png'),
      subject: 'English',
    },
  ]);

  return (
    <SafeAreaView style={styles.container}>
      <LinearHeader>
        <View style={styles.viewHeader}>
          <TouchableOpacity
            style={styles.touchBack}
            onPress={navigation.goBack}>
            <Image
              source={require('../images/back.png')}
              style={styles.imageBack}
            />
          </TouchableOpacity>
          <Text style={styles.textTitle}>My Cards</Text>
          <TouchableOpacity style={styles.touchNew}>
            <Text style={styles.textNew}>+New</Text>
          </TouchableOpacity>
        </View>
      </LinearHeader>
      <FlatList
        numColumns={2}
        data={people}
        renderItem={({item}) => {
          console.log(JSON.stringify(item, null, 2));
          return (
            <TouchableOpacity
              onPress={() => navigation.navigate(item.nav)}
              style={styles.flatview}>
              <ImageBackground source={item.source} style={styles.flatimage}>
                <View style={styles.flatview1}>
                  <Text style={styles.flattxt}>{item.subject}</Text>
                </View>
              </ImageBackground>
            </TouchableOpacity>
          );
        }}
      />
    </SafeAreaView>
  );
};

export default MyCards;
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  viewHeader: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 20,
  },
  textTitle: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 20,
    color: '#FFFFFF',
  },
  touchBack: {
    position: 'absolute',
    left: 0,
  },
  touchNew: {
    position: 'absolute',
    right: 0,
  },
  textNew: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 20,
    color: '#FFFFFF',
  },
  imageBack: {
    height: 40,
    width: 40,
    resizeMode: 'contain',
  },
  flatimage: {
    width: 160,
    height: 150,
    resizeMode: 'contain',
    alignSelf: 'center',
  },
  flatview: {
    width: '50%',
    marginTop: 20,
  },
  flatview1: {
    backgroundColor: '#000000aa',
    width: '100%',
    position: 'absolute',
    bottom: 0,
    padding: 5,
    borderBottomEndRadius: 17,
    borderBottomStartRadius: 17,
  },
  flattxt: {
    fontFamily: 'Avenir-Medium',
    fontSize: 18,
    color: '#FFFFFF',
    fontWeight: '500',
    marginLeft: 20,
  },
});

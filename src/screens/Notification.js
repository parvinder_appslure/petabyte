import React from 'react';
import {useState} from 'react';
import {Image, StyleSheet, Text, View, FlatList} from 'react-native';
import {SimpleHeader, MainView} from '../utils/Header';

const Notification = ({navigation, route}) => {
  const [person, setPerson] = useState([
    {
      title: 'ADITYA',
      today: 'You missed your revision session',
      time: '20 min ago',
      source: require('../images/notification-1.png'),
      key: 1,
    },
  ]);

  return (
    <MainView>
      <SimpleHeader title={'Notification'} onPress={navigation.goBack} />
      <FlatList
        data={person}
        renderItem={({item}) => {
          console.log(JSON.stringify(item, null, 2));
          return (
            <View style={styles.flatStyle}>
              <View style={styles.viewStyle}>
                <Image source={item.source} style={styles.flatimage} />
                <View style={styles.view1Style}>
                  <Text style={styles.flattext}>{item.title}</Text>
                  <Text style={styles.flattext1}>{item.today}</Text>
                  <Text style={styles.flattext1}>{item.time}</Text>
                </View>
              </View>
            </View>
          );
        }}
      />
    </MainView>
  );
};

export default Notification;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  flatStyle: {
    borderRadius: 15,
    backgroundColor: '#FFFFFF',
    width: '85%',
    alignSelf: 'center',
    shadowColor: '#000',

    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    marginTop: 20,
    elevation: 5,
    marginBottom: 10,
    paddingBottom: 20,
  },

  flatimage: {width: 60, height: 60, resizeMode: 'contain'},
  viewStyle: {
    flexDirection: 'row',
    width: '80%',
    alignSelf: 'center',
    marginTop: 20,
    justifyContent: 'flex-start',
  },
  flattext: {
    fontFamily: 'Avenir-Medium',
    fontSize: 16,
    color: '#000000',
    fontWeight: '500',
    marginTop: 5,
  },
  view1Style: {
    flexDirection: 'column',
    marginLeft: 20,
    width: '70%',
  },
  flattext1: {
    fontFamily: 'Avenir-Medium',
    fontSize: 16,
    color: '#8F8F8F',
    fontWeight: '500',
    marginTop: 5,
  },
});

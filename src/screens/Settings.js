import React, {useState} from 'react';
import {
  Image,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {Switch} from 'react-native-switch';
import RadioForm, {
  RadioButton,
  RadioButtonInput,
  RadioButtonLabel,
} from 'react-native-simple-radio-button';
import {SimpleHeader} from '../utils/Header';

const arrowImage = require('../images/Fw.png');

const Settings = ({navigation, route}) => {
  const [state, setState] = useState({
    mode: false,
    notification: false,
  });

  const [notificationState, setNotificationState] = useState(0);
  const [languageState, setLanguageState] = useState(0);
  const notification_props = [
    {label: 'Sound', value: 0},
    {label: 'Vibration Only', value: 1},
    {label: 'Sound & Vibration', value: 2},
  ];
  const language_props = [
    {label: 'Hindi', value: 0},
    {label: 'English', value: 1},
  ];

  const notifcationStateHandler = value => setNotificationState(value);
  const languageStateHandler = value => setLanguageState(value);
  const RadioFormView = (list, state, onPress) => (
    <RadioForm formHorizontal={false} animation={true} style={{marginTop: 10}}>
      {list.map((obj, i) => (
        <RadioButton labelHorizontal={true} key={i} style={radioStyle.style}>
          <RadioButtonLabel
            obj={obj}
            index={i}
            labelHorizontal={true}
            onPress={onPress}
            labelStyle={radioStyle.labelStyle}
            labelWrapStyle={radioStyle.labelWrapStyle}
          />
          <RadioButtonInput
            obj={obj}
            index={i}
            isSelected={state === i}
            onPress={onPress}
            borderWidth={1.5}
            buttonInnerColor={'#F23A59'}
            buttonOuterColor={state === i ? '#F23A59' : '#0000004d'}
            buttonSize={10}
            buttonOuterSize={20}
            buttonStyle={{}}
            buttonWrapStyle={radioStyle.buttonWrapStyle}
          />
        </RadioButton>
      ))}
    </RadioForm>
  );

  const CardView = props => (
    <View style={styles.viewCardContainer} {...props} />
  );
  const updateStatus = (key, value) => {
    console.log(key, value);
    setState({...state, [key]: value});
  };
  const SwitchView = key => (
    <Switch
      barHeight={25}
      value={state[key]}
      onValueChange={value => updateStatus(key, value)}
      circleSize={15}
      circleBorderWidth={0.5}
      innerCircleStyle={styles.switch_icStyle}
      switchLeftPx={1.5}
      switchRightPx={1.2}
      switchWidthMultiplier={3.5}
      switchBorderRadius={25}
      activeTextStyle={styles.switch_text}
      inactiveTextStyle={styles.switch_text}
      backgroundActive={'#F23A59'}
      renderActiveText={false}
      renderInActiveText={false}
      // backgroundInactive={'grey'}
    />
  );
  const optionView = (title, source) => (
    <CardView>
      <View style={styles.viewAboutContainer}>
        <Image style={styles.imageIcon} source={source} />
        <Text style={styles.textTitle}>{title}</Text>
        <TouchableOpacity style={styles.touchAbout}>
          <Image style={styles.imageArrow} source={arrowImage} />
        </TouchableOpacity>
      </View>
    </CardView>
  );
  return (
    <SafeAreaView style={styles.container}>
      <SimpleHeader title={'Settings'} onPress={navigation.goBack} />
      <ScrollView>
        <CardView>
          <View style={styles.viewTitleConstainer}>
            <Text style={styles.textTitle}>Sleep Mode</Text>
            {SwitchView('mode')}
          </View>
          <Text style={styles.textInfo}>
            In sleep mode you will not receive any notification or wake call
          </Text>
        </CardView>

        <CardView>
          <Text style={styles.textTitle}>Notification</Text>
          {RadioFormView(
            notification_props,
            notificationState,
            notifcationStateHandler,
          )}
        </CardView>
        <CardView>
          <View style={styles.viewTitleConstainer}>
            <Text style={styles.textTitle}>Push Notification</Text>
            {SwitchView('notification')}
          </View>
          <Text style={styles.textInfo}>
            If you make push notification off you will not receive any kind of
            notification releated sessions and other information
          </Text>
        </CardView>
        <CardView>
          <View style={styles.viewAboutContainer}>
            <Text style={styles.textTitle}>Alarm</Text>
            <TouchableOpacity style={styles.touchAbout}>
              <Image style={styles.imageArrow} source={arrowImage} />
            </TouchableOpacity>
          </View>
        </CardView>
        <CardView>
          <Text style={styles.textTitle}>Language for course</Text>
          {RadioFormView(language_props, languageState, languageStateHandler)}
        </CardView>

        {optionView('About Us', require('../images/about.png'))}
        {optionView('Term & Conditions', require('../images/privacy.png'))}
        {optionView('Term & Privacy', require('../images/terms.png'))}
        {optionView('Help center', require('../images/help.png'))}
        <View style={{margin: 10}} />
      </ScrollView>
    </SafeAreaView>
  );
};

export default Settings;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  viewCardContainer: {
    backgroundColor: 'white',
    marginHorizontal: 20,
    marginTop: 20,
    padding: 20,
    borderRadius: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  viewTitleConstainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 10,
  },
  textTitle: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 17,
    color: '#000000',
  },
  textInfo: {
    fontFamily: 'Avenir-Roman',
    fontWeight: '400',
    fontSize: 14,
    color: '#0000004d',
  },
  textInfo_2: {
    fontFamily: 'Avenir-Roman',
    fontWeight: '400',
    fontSize: 17,
    color: '#00000080',
  },

  switch_icStyle: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
  },
  switch_text: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 9,
    fontWeight: '900',
    color: '#fff',
  },
  viewAboutContainer: {
    flexDirection: 'row',
    // justifyContent: 'space-between',
    alignItems: 'center',
  },
  touchAbout: {
    padding: 5,
    marginLeft: 'auto',
  },
  imageArrow: {
    height: 15,
    width: 10,
    // resizeMode: 'contain',
  },
  imageIcon: {
    height: 30,
    width: 30,
    borderRadius: 6,
    marginRight: 15,
  },
});

export const radioStyle = StyleSheet.create({
  style: {
    justifyContent: 'space-between',
    paddingVertical: 10,
    alignItems: 'center',
  },

  labelStyle: {
    fontFamily: 'Avenir-Roman',
    fontWeight: '400',
    fontSize: 17,
    color: '#00000080',
  },
  buttonWrapStyle: {},
  labelWrapStyle: {},
});

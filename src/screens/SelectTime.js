import React, {useEffect, useState} from 'react';

import {
  Image,
  ImageBackground,
  Text,
  View,
  StyleSheet,
  Dimensions,
  Modal,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import {useStore} from 'react-redux';
import {StatusBarLight} from '../utils/CustomStatusBar';

const {width, height} = Dimensions.get('window');

const SelectTime = ({navigation}) => {
  const {preferedTiming} = useStore().getState();
  const [state, setState] = useState({
    timingId: '',
  });

  const onSelectHandler = ({id}) => {
    setState({...state, timingId: id});
    navigation.navigate('schoolUserType', {
      timingId: id,
    });
  };
  return (
    <>
      <View style={{backgroundColor: 'pink', flex: 1}}>
        <StatusBarLight />
        <ImageBackground
          source={require('../images/bg.png')}
          style={styles.i_Style}>
          <View style={styles.v_Style}>
            <Text style={styles.t_Style}>
              Choose Your Preffered Timing To Use The App
            </Text>
            <FlatList
              data={preferedTiming}
              renderItem={({item}) => {
                const {id, title} = item;
                return (
                  <TouchableOpacity
                    key={`key_${id}`}
                    style={styles.v1_Style}
                    onPress={() => onSelectHandler(item)}>
                    <Text style={styles.t3_Style}>{title}</Text>
                    {id === state.timingId && (
                      <Image
                        source={require('../images/check-1.png')}
                        style={styles.i3_Style}
                      />
                    )}
                  </TouchableOpacity>
                );
              }}
              keyExtractor={item => item.id.toString()}
            />
          </View>
        </ImageBackground>
      </View>
    </>
  );
};

export default SelectTime;

const styles = StyleSheet.create({
  i_Style: {
    flex: 1,
  },
  v_Style: {
    width: '90%',
    alignSelf: 'center',
  },
  i2_Style: {
    width: 40,
    height: 40,
    resizeMode: 'contain',
    position: 'absolute',
    marginTop: 40,
    marginLeft: 20,
  },
  t_Style: {
    fontFamily: 'Avenir',
    fontSize: 34,
    fontWeight: 'bold',
    color: '#FFFFFF',
    marginTop: 100,
    alignSelf: 'center',
  },
  v1_Style: {
    backgroundColor: '#FFFFFF',
    borderRadius: 20,
    width: '94%',
    alignSelf: 'center',
    marginTop: 40,
    paddingVertical: 12,
    paddingHorizontal: 30,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  t2_Style: {
    fontFamily: 'Avenir',
    fontSize: 18,
    fontWeight: '500',
    color: '#FF2D55',
  },
  i3_Style: {
    width: 30,
    height: 30,
    resizeMode: 'contain',
  },
  v2_Style: {
    width: '90%',
    alignSelf: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  t3_Style: {
    fontFamily: 'Avenir',
    fontSize: 18,
    fontWeight: '500',
    color: '#FF2D55',
    paddingVertical: 5,
  },

  madal_View: {
    flex: 1,
    backgroundColor: '#000000aa',
  },
  madal_Style: {
    backgroundColor: '#FFFFFF',
    width: '90%',
    alignSelf: 'center',
    //borderRadius: 20,
    marginTop: 200,
    //justifyContent: 'center',
  },
  mdt_style: {
    fontFamily: 'Avenir',
    fontSize: 22,
    fontWeight: 'bold',
    color: '#000000',
  },
  mdt1_Style: {
    fontFamily: 'Avenir',
    fontSize: 17,
    fontWeight: '400',
    color: '#1D1E2C',
  },
  mdv_Style: {
    marginTop: 10,
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-between',
  },
  mdv1_Style: {
    width: '25%',
    backgroundColor: '#7ED321',
    alignItems: 'center',
    borderRadius: 10,
    padding: 7,
    marginTop: -5,
  },
  mdt1_Style: {
    fontFamily: 'Avenir',
    fontSize: 14,
    fontWeight: '500',
    color: '#FFFFFF',
  },
  mdv2_Style: {
    borderColor: '#EDEEEF',
    borderWidth: 0.5,
    marginTop: 15,
  },
  mdt2_Style: {
    fontFamily: 'Avenir',
    fontSize: 16,
    fontWeight: '400',
    color: '#6F6F7B',
    marginTop: 10,
  },
  mdv3_Style: {
    padding: 20,
  },
  mdv4_Style: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  mdv5_Style: {
    width: '100%',
    backgroundColor: '#FFD00D',
    marginTop: 20,
    padding: 10,
    //alignItems: 'center',
  },
});

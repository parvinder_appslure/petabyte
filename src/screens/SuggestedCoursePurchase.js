import React, {useEffect, useState} from 'react';
import {
  ScrollView,
  View,
  TouchableOpacity,
  Image,
  Text,
  StyleSheet,
} from 'react-native';
import {LinearHeader, MainView, SubmitButton} from '../utils/Header';
import {StatusBarLight} from '../utils/CustomStatusBar';
import {PriceFormat} from '../utils/CommonFunction';
import {CPaymentApi} from '../services/Api';
import {useSelector} from 'react-redux';

const SuggestedCoursePurchase = ({navigation, route}) => {
  const {userDetail} = useSelector(state => state);
  const [state, setState] = useState({
    ...route.params,
  });
  const onSubmitHandler = async () => {
    // console.log('adsf');
    // consolejson(state);
    const {user_id, email, mobile} = userDetail;
    const {name, id, price, discount_per} = state;

    let bol = +discount_per === 0;
    let discount = price - (price * discount_per) / 100;
    let amount = bol ? price : discount;
    const body = {
      module: 'course',
      payment_id: `OID_${user_id}_${new Date().getTime()}`,
      amount,
      currency: 'INR',
      description: name,
      created_at: '121212',
      method: 'card',
      email,
      contact: mobile,
      fee: '0',
      tax: '0',
      user_id,
      plan_id: '8',
      course_id: id,
      tax_amount: '0',
    };
    const response = await CPaymentApi(body);
    const {status = false, message = 'Something went wrong'} = response;
    if (status) {
      alert('Course Purchased Successfully');
    } else {
      alert(message);
    }

    // {
    //     "status": "1",
    //     "user_id": "133",
    //     "email": "sachinappslu1re11@gmail.com",
    //     "name": "Sachin Applsure",
    //     "father_name": "father",
    //     "mother_name": "mother",
    //     "mobile": "8290838111811",
    //     "image": "http://139.59.76.223/Acecodes/uploads/student/default.jpg",
    //     "dob": "2020-10-10",
    //     "gender": "Male",
    //     "address": "",
    //     "state": "ANDHRA PRADESH",
    //     "city": "Rohini",
    //     "language": "en",
    //     "preferred_timing": "6am-10pm",
    //     "type": "1"
    //   }
  };
  useEffect(() => {
    consolejson(state);
  }, []);
  return (
    <MainView>
      <StatusBarLight />
      <ScrollView>
        <LinearHeader>
          <View style={styles.headerContainer}>
            <TouchableOpacity
              style={styles.touchBack}
              onPress={navigation.goBack}>
              <Image
                source={require('../images/back.png')}
                style={styles.imageBack}
              />
            </TouchableOpacity>
            <View style={styles.viewTitle}>
              <Text style={styles.titleText}>{state.name}</Text>
              <OptionPrice
                price={state.price}
                percentage={state.discount_per}
              />
            </View>
          </View>
        </LinearHeader>
        <SubmitButton title={'pay'} onPress={onSubmitHandler} />
      </ScrollView>
    </MainView>
  );
};

export default SuggestedCoursePurchase;

const styles = StyleSheet.create({
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 15,
  },
  touchBack: {
    position: 'absolute',
    left: 0,
  },
  imageBack: {
    height: 40,
    width: 40,
    resizeMode: 'contain',
  },
  titleText: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 20,
    color: '#FFFFFF',
  },
  viewTitle: {
    marginHorizontal: 40,
  },
});

const OptionPrice = ({price, percentage}) => {
  let bol = +percentage === 0;
  let discount = price - (price * percentage) / 100;
  return (
    <Text style={[styles.titleText, {marginTop: 5}]}>
      {PriceFormat(bol ? price : discount)}
    </Text>
  );
};

import React, {useEffect, useState} from 'react';
import {
  Dimensions,
  StyleSheet,
  Image,
  Text,
  View,
  ScrollView,
} from 'react-native';
import {useSelector} from 'react-redux';
import {CDApi} from '../services/Api';
import {PriceFormat} from '../utils/CommonFunction';
import {StatusBarLight} from '../utils/CustomStatusBar';
import {
  MainView,
  SimpleHeader,
  SubmitButton,
  RatingView,
} from '../utils/Header';
import Loader from '../utils/Loader';
const {width, height} = Dimensions.get('window');
import HTMLView from 'react-native-htmlview';

const SuggestedCourseDetails = ({navigation, route}) => {
  const {userDetail} = useSelector(state => state);
  const [state, setState] = useState({
    id: '',
    name: '',
    description: '',
    price: '',
    discount_per: '',
    image: '',
    includes: [],
    plans: [],
    loading: true,
  });

  const onSubmitHandler = () => {
    const {id, name, price, discount_per} = state;
    navigation.navigate('SuggestedCoursePurchase', {
      id,
      name,
      price,
      discount_per,
    });
  };
  useEffect(() => {
    (async () => {
      const response = await CDApi({
        course_id: route.params.id,
        user_id: userDetail.user_id,
      });
      const {
        status = false,
        id,
        name,
        description,
        price,
        discount_per,
        image,
        includes,
        plans,
      } = response;
      if (status) {
        setState({
          ...state,
          id,
          name,
          description,
          price,
          discount_per,
          image,
          includes,
          plans,
          loading: false,
        });
      } else {
        setState({...state, loading: false});
      }
    })();
  }, []);
  return (
    <MainView>
      <StatusBarLight />
      <Loader status={state.loading} />
      <ScrollView>
        <SimpleHeader title={state.name} onPress={navigation.goBack} />
        <Image
          source={require('../images/suggested.png')}
          // source={{uri:state.image}}
          style={styles.image}
        />
        <Text style={styles.title}>{state.name}</Text>

        <RatingView
          defaultRating={route.params.ratings}
          starContainerStyle={styles.starContainerStyle}
        />
        <OptionPrice price={state.price} percentage={state.discount_per} />
        <View style={styles.hzLine} />
        <SubTitle title={'Description'} />
        <HTMLView value={state.description} stylesheet={htmlStyles} />
        <SubTitle title={'This Course Includes'} />
        {state.includes.map(({title}) => (
          <TextIncludes title={`\u2022 ${title}`} />
        ))}
        <SubmitButton title={'Buy Now'} onPress={onSubmitHandler} />
        <View style={{marginTop: 40}} />
      </ScrollView>
    </MainView>
  );
};
export default SuggestedCourseDetails;

const styles = StyleSheet.create({
  image: {
    width,
    height: 180,
  },
  title: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 25,
    color: '#1E2432',
    marginHorizontal: 20,
    marginTop: 15,
  },
  subTitle: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 20,
    color: '#1E2432',
    marginHorizontal: 20,
    marginVertical: 10,
  },
  hzLine: {
    backgroundColor: '#D8D8D8',
    width: '90%',
    height: 1,
    alignSelf: 'center',
    margin: 10,
  },
  viewPriceContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 20,
  },
  textPrice_1: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    fontSize: 25,
    color: '#F33F51',
    marginRight: 10,
  },
  textPrice_2: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 16,
    color: '#C7C7CC',
    textDecorationLine: 'line-through',
  },
  textIncludesTitle: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 16,
    color: '#000000',
    marginHorizontal: 30,
    marginVertical: 5,
  },
  starContainerStyle: {
    marginHorizontal: 20,
    marginVertical: 5,
  },
});

const htmlStyles = StyleSheet.create({
  p: {
    marginHorizontal: 20,
    color: 'black',
  },
  h2: {
    fontSize: 14,
    marginHorizontal: 30,
  },
  ul: {
    fontSize: 12,
    marginHorizontal: 40,
  },
});

const OptionPrice = ({price, percentage}) => {
  let bol = +percentage === 0;
  let discount = price - (price * percentage) / 100;

  return (
    <View style={styles.viewPriceContainer}>
      <Text style={styles.textPrice_1}>
        {PriceFormat(bol ? price : discount)}
      </Text>
      {!bol && <Text style={styles.textPrice_2}>{PriceFormat(price)}</Text>}
    </View>
  );
};
const SubTitle = props => <Text style={styles.subTitle}>{props.title}</Text>;
const TextIncludes = props => (
  <Text style={styles.textIncludesTitle}>{props.title}</Text>
);

import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  View,
  Dimensions,
  ImageBackground,
  Text,
  Image,
  TouchableOpacity,
} from 'react-native';
import {StatusBarLight} from '../utils/CustomStatusBar';
const {width, height} = Dimensions.get('window');
const schoolUserType = ({navigation, route}) => {
  const [state, setState] = useState({
    userType: '',
  });
  useEffect(() => {}, []);

  const onSelectHandler = userType => {
    setState({...state, userType});
    const {timingId} = route.params;
    if (userType === 'individual') {
      navigation.navigate('nuWelcome', {userType, timingId});
    }
    if (userType === 'student') {
      navigation.navigate('NULogin', {userType, timingId});
    }
  };

  const userTypeView = (title, userType, source) => {
    return (
      <TouchableOpacity
        onPress={() => onSelectHandler(userType)}
        style={{alignSelf: 'center', marginTop: 50}}>
        <Image source={source} style={styles.i_Style} />
        <Text style={styles.t1_Style}>{title}</Text>
        {userType === state.userType && (
          <Image
            source={require('../images/check-1.png')}
            style={styles.i2_Style}
          />
        )}
      </TouchableOpacity>
    );
  };
  return (
    <View style={{flex: 1, backgroundColor: 'red'}}>
      <StatusBarLight />
      <ImageBackground source={require('../images/bg.png')} style={{flex: 1}}>
        <Text style={styles.t_Style}>{`Please Select a \nUser Type`}</Text>
        {userTypeView(
          'School User',
          'student',
          require('../images/student.png'),
        )}
        {userTypeView(
          'Individual User',
          'individual',
          require('../images/student-2.png'),
        )}
      </ImageBackground>
    </View>
  );
};

export default schoolUserType;

const styles = StyleSheet.create({
  t_Style: {
    fontFamily: 'Avenir',
    fontSize: 35,
    fontWeight: 'bold',
    color: '#FFFFFF',
    marginTop: 80,
    alignSelf: 'center',
    marginRight: 20,
  },
  i_Style: {
    alignSelf: 'center',
    width: 150,
    height: 150,
    resizeMode: 'contain',
    marginBottom: 15,
  },
  i2_Style: {
    width: 45,
    height: 45,
    position: 'absolute',
    right: 0,
    top: 0,
  },
  v_Style: {
    flexDirection: 'row',
    alignSelf: 'center',
  },
  t1_Style: {
    fontFamily: 'Avenir',
    fontSize: 21,
    fontWeight: 'bold',
    color: '#FFFFFF',
    textAlign: 'center',
  },
});

import React, {useEffect, useState} from 'react';

import {Dimensions, StyleSheet, ScrollView, TextInput} from 'react-native';

import {StatusBarLight} from '../utils/CustomStatusBar';
import {globStyle} from '../style/style';
import {
  BackButton,
  BackgroundImage,
  FormView,
  H1,
  H2,
  MainView,
  SubmitButton,
} from '../utils/Header';
import {CPApi} from '../services/Api';

const {width, height} = Dimensions.get('window');

const CreatePassword = ({navigation, route}) => {
  const [state, setState] = useState({
    password: '',
    confirmPassword: '',
  });
  useEffect(() => {
    // consolejson(route.params);
    consolejson(state);
  }, [state]);
  const onSubmitHandler = async () => {
    consolejson(state);
    const {password, confirmPassword} = state;
    const {userType, body} = route.params;
    if (password === '') return;
    if (password === confirmPassword) {
      console.log('change password', body);
      const {email_mobile} = body;
      const response = await CPApi({email_mobile, password});
      const {status = false, msg = ''} = response;
      if (status) {
        alert('Password has been change Successfully');
        navigation.pop(3);
        // consolejson(Object.keys(navigation));
      } else {
        alert(msg);
      }
    } else {
      alert('Your password does not match');
    }
  };
  return (
    <MainView>
      <StatusBarLight />
      <BackgroundImage>
        <ScrollView>
          <BackButton onPress={navigation.goBack} />
          <H1 text={'Create password'} />
          <H2 text={'Please enter your new password'} />
          <FormView>
            <TextInput
              placeholder="New Password"
              underlineColorAndroid="#EAECEF"
              value={state.password}
              secureTextEntry={true}
              onChangeText={password => setState({...state, password})}
              style={globStyle.textInput}
            />
            <TextInput
              placeholder="Confirm Password"
              underlineColorAndroid="#EAECEF"
              value={state.confirmPassword}
              secureTextEntry={true}
              onChangeText={confirmPassword =>
                setState({...state, confirmPassword})
              }
              style={globStyle.textInput}
            />
            <SubmitButton onPress={onSubmitHandler} title={'Submit'} />
          </FormView>
        </ScrollView>
      </BackgroundImage>
    </MainView>
  );
};

export default CreatePassword;

const styles = StyleSheet.create({});

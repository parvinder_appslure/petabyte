import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  Dimensions,
  StyleSheet,
  ImageBackground,
  Image,
  TouchableOpacity,
  ScrollView,
  TextInput,
  Modal,
} from 'react-native';

import {StatusBarLight} from '../utils/CustomStatusBar';
import {globStyle} from '../style/style';
import {
  BackButton,
  BackgroundImage,
  FormView,
  H1,
  H2,
  MainView,
  SubmitButton,
} from '../utils/Header';
import {FPApi, FPSApi} from '../services/Api';
import {GenerateOtp} from '../utils/CommonFunction';
const {width, height} = Dimensions.get('window');

const ForgetPassword = ({navigation, route}) => {
  const [modalOpen, setModalOpen] = useState(false);

  const [state, setState] = useState({
    email_mobile: '8290838111811',
    uid: '123123',
    class_id: '7',
    roll_number: '1234',
  });
  useEffect(() => {
    consolejson(route.params);
  });

  const onSubmitHandler = async () => {
    const {userType} = route.params;
    const {email_mobile, uid, class_id, roll_number} = state;
    const otp = GenerateOtp();
    if (userType === 'student') {
      const body = {
        email_mobile: '8290838111811',
        uid: '123123',
        class_id: '7',
        roll_number: '1234',
        otp,
      };
      const {status = false, msg = ''} = await FPSApi(body);
      if (status) {
        navigation.navigate('Otp', {
          ...route.params,
          body,
        });
      } else {
        alert(msg);
      }
    }
    if (userType === 'individual') {
      const body = {
        email_mobile: '8290838118',
        otp,
      };
      const {status = false, msg = ''} = await FPApi(body);
      if (status) {
        navigation.navigate('Otp', {
          ...route.params,
          body,
        });
      } else {
        alert(msg);
      }
    }
  };
  return (
    <MainView>
      <StatusBarLight />
      <BackgroundImage>
        <ScrollView>
          <BackButton onPress={navigation.goBack} />
          <H1 text={'Forgot password'} />
          <H2 text={'Please enter your email address/mobile no.'} />
          <FormView>
            {route.params.userType === 'individual' && (
              <TextInput
                placeholder="Email/Mobile"
                underlineColorAndroid="#EAECEF"
                value={state.email_mobile}
                onChangeText={email_mobile =>
                  setState({...setState, email_mobile})
                }
                style={globStyle.textInput}
              />
            )}
            {route.params.userType === 'student' && (
              <>
                <TextInput
                  placeholder="Acecodes ID"
                  underlineColorAndroid="#EAECEF"
                  value={state.uid}
                  onChangeText={uid => setState({...setState, uid})}
                  style={globStyle.textInput}
                />
                <TextInput
                  placeholder="Mobile No"
                  underlineColorAndroid="#EAECEF"
                  value={state.mobile}
                  keyboardType={'number-pad'}
                  onChangeText={email_mobile =>
                    setState({...setState, email_mobile})
                  }
                  style={globStyle.textInput}
                />
                <TextInput
                  placeholder="Class"
                  underlineColorAndroid="#EAECEF"
                  value={state.mobile}
                  keyboardType={'default'}
                  onChangeText={class_id => setState({...setState, class_id})}
                  style={globStyle.textInput}
                />
                <TextInput
                  placeholder="Roll No"
                  underlineColorAndroid="#EAECEF"
                  value={state.roll_number}
                  keyboardType={'default'}
                  onChangeText={roll_number =>
                    setState({...setState, roll_number})
                  }
                  style={globStyle.textInput}
                />
              </>
            )}
            <SubmitButton onPress={onSubmitHandler} title={'Submit'} />
          </FormView>
        </ScrollView>
      </BackgroundImage>
    </MainView>
  );
};

export default ForgetPassword;

const styles = StyleSheet.create({});

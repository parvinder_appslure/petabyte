import React, {useEffect} from 'react';
import {
  StyleSheet,
  View,
  Dimensions,
  ImageBackground,
  Text,
} from 'react-native';
import {useDispatch} from 'react-redux';
import {
  AsyncStorageGetUserId,
  GetProfileApi,
  MPTApi,
  MSApi,
} from '../services/Api';
import * as actions from '../redux/actions';
import {MainView} from '../utils/Header';
import {StatusBarLight} from '../utils/CustomStatusBar';
const {width, height} = Dimensions.get('window');
const Splash = ({navigation, route}) => {
  const dispatch = useDispatch();
  useEffect(() => {
    (async () => {
      const {userId, userType} = JSON.parse(await AsyncStorageGetUserId()) || {
        userId: '',
        userType: '',
      };
      console.log({userId, userType});
      if (userId === '') {
        const mtpPromise = new Promise(async resolve => {
          const {status = false, list = []} = await MPTApi();
          status
            ? dispatch(actions.preferedTiming(list))
            : console.log('error');
          resolve(status);
        });

        const msPromise = new Promise(async resolve => {
          const {status = false, list = []} = await MSApi();
          status ? dispatch(actions.masterStates(list)) : console.log('error');
          resolve(status);
        });
        Promise.all([mtpPromise, msPromise]).then(result => {
          if (result.includes(false)) {
            alert('Something went wrong');
          } else {
            navigation.replace('SelectTime');
          }
        });
      } else {
        const response = await GetProfileApi({user_id: userId});
        const {status = false, user_details = {}} = response;
        if (status) {
          dispatch(actions.userDetail({userDetail: user_details, userType}));
          navigation.replace('DrawerNavigator');
        } else {
          alert('Something went wrong');
          consolejson(response);
        }
      }

      // const {status = false, list = []} = await MPTApi();
      // if (status) {
      //   dispatch(actions.preferedTiming(list));
      //   navigation.replace('SelectTime');
      // } else {
      //   console.log('error');
      // }
    })();
  }, []);

  useEffect(() => {
    console.log('splash');
  }, []);
  return (
    <MainView>
      <StatusBarLight />
      <ImageBackground
        source={require('../images/splash-screen.png')}
        style={styles.container}></ImageBackground>
    </MainView>
  );
};

export default Splash;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  mainLogo: {
    height: 170,
    width: 210,
    resizeMode: 'contain',
    position: 'absolute',
  },
  bottomView: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 'auto',
    marginBottom: 20,
  },
  text: {
    fontFamily: 'Avenir',
    fontWeight: '500',
    fontSize: 1,
    color: '#FFFFFF',
  },
  bottomLogo: {
    height: 45,
    width: 100,
    resizeMode: 'contain',
    marginLeft: 5,
  },
});

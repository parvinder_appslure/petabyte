import React, {useEffect, useState} from 'react';

import {
  View,
  Text,
  Dimensions,
  StyleSheet,
  ImageBackground,
  Image,
  TouchableOpacity,
  ScrollView,
  TextInput,
  Modal,
} from 'react-native';

import {StatusBarLight} from '../utils/CustomStatusBar';

const {width, height} = Dimensions.get('window');

const SelectCourse = ({navigation}) => {
  return (
    <View style={styles.i_Style}>
      <StatusBarLight />
      <ImageBackground
        source={require('../images/bg.png')}
        style={styles.i_Style}>
        <ScrollView>
          <View style={styles.v_Style}>
            <Text style={styles.t_Style}>{`Choose your \nCourses`}</Text>

            <Text style={styles.t2_Style}>Classes 5-12</Text>

            <View style={styles.v1Style}>
              <View style={styles.v2Style}>
                <Text style={styles.t3_Style}>5th</Text>
              </View>

              <View style={styles.v2Style}>
                <Text style={styles.t3_Style}>6th</Text>
              </View>

              <View style={styles.v2Style}>
                <Text style={styles.t3_Style}>7th</Text>
              </View>
            </View>

            <View style={styles.v1Style}>
              <View style={styles.v2Style}>
                <Text style={styles.t3_Style}>8th</Text>
              </View>

              <View style={styles.v2Style}>
                <Text style={styles.t3_Style}>9th</Text>
              </View>

              <View style={styles.v2Style}>
                <Text style={styles.t3_Style}>10th</Text>
              </View>
            </View>

            <View style={styles.v3Style}>
              <View style={styles.v2Style}>
                <Text style={styles.t3_Style}>11th</Text>
              </View>

              <TouchableOpacity
                onPress={() => navigation.navigate('SubscriptionDetails')}
                style={styles.v2Style}>
                <Text style={styles.t3_Style}>12th</Text>
              </TouchableOpacity>
            </View>

            <View style={styles.v4Style}>
              <Text style={styles.t3_Style}>11th Grade-Commerce</Text>
            </View>
            <View style={styles.v4Style}>
              <Text style={styles.t3_Style}>12th Grade-Commerce</Text>
            </View>

            <Text style={styles.t2_Style}>Exams Preprations</Text>

            <View style={styles.v3Style}>
              <View style={styles.v2Style}>
                <Text style={styles.t3_Style}>CAT</Text>
              </View>

              <View style={styles.v2Style}>
                <Text style={styles.t3_Style}>IAS</Text>
              </View>
            </View>
          </View>
        </ScrollView>
      </ImageBackground>
    </View>
  );
};

export default SelectCourse;

const styles = StyleSheet.create({
  i_Style: {
    flex: 1,
  },
  v_Style: {
    width: '85%',
    alignSelf: 'center',
    //backgroundColor: 'yellow',
  },
  t_Style: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 35,
    color: '#FFFFFF',
    marginTop: 50,
    lineHeight: 50,
  },
  t2_Style: {
    fontFamily: 'Avenir-Roman',
    fontSize: 22,
    fontWeight: '500',
    color: '#FFFFFF',
    marginTop: 25,
  },
  t3_Style: {
    fontFamily: 'Avenir-Roman',
    fontSize: 22,
    fontWeight: '800',
    color: '#FF2D55',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  v1Style: {
    flexDirection: 'row',
    width: '100%',
    alignSelf: 'center',
    marginTop: 30,
    justifyContent: 'space-around',
  },
  v2Style: {
    width: '30%',
    alignSelf: 'center',
    backgroundColor: '#FFFFFF',
    padding: 10,
    borderRadius: 20,
  },
  v3Style: {
    flexDirection: 'row',
    width: '70%',
    //alignSelf: 'center',
    marginTop: 30,
    justifyContent: 'space-evenly',
    marginLeft: -20,
  },
  v4Style: {
    width: '90%',
    //alignSelf: 'center',
    backgroundColor: '#FFFFFF',
    padding: 10,
    borderRadius: 20,
    marginTop: 30,
  },
});

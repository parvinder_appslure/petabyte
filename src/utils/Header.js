import React from 'react';
import {
  Image,
  Platform,
  StatusBar,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ImageBackground,
  SafeAreaView,
  Dimensions,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

const {width, height} = Dimensions.get('window');
import {AirbnbRating} from 'react-native-ratings';
const paddingTop =
  Platform.OS === 'android' ? StatusBar.currentHeight + 15 : 10;

export const LinearHeader = props => (
  <LinearGradient
    colors={['#F54B3A', '#F13268']}
    start={{x: 0, y: 1}}
    end={{x: 1, y: 1}}
    style={[headerStyle.linearGradient]}
    {...props}
  />
);
export const LinearView = props => (
  <LinearGradient
    colors={LinearColor[props.index]}
    start={{x: 1, y: 1}}
    end={{x: 1, y: 0}}
    {...props}
  />
);
export const LinearViewTwo = props => (
  <LinearGradient start={{x: 1, y: 1}} end={{x: 0, y: 1}} {...props} />
);
export const SimpleHeader = props => (
  <LinearHeader>
    <View style={headerStyle.viewHeader}>
      <TouchableOpacity style={headerStyle.touchBack} onPress={props.onPress}>
        <Image
          source={require('../images/back.png')}
          style={headerStyle.imageBack}
        />
      </TouchableOpacity>
      <Text style={headerStyle.textTitle}>{props.title}</Text>
    </View>
  </LinearHeader>
);
export const SimpleHeaderTwo = props => (
  <View style={headerStyle.linearGradient}>
    <View style={headerStyle.viewHeader}>
      <TouchableOpacity style={headerStyle.touchBack} onPress={props.onPress}>
        <Image
          source={require('../images/back.png')}
          style={headerStyle.imageBack}
        />
      </TouchableOpacity>
      <Text style={headerStyle.textTitle}>{props.title}</Text>
    </View>
  </View>
);

export const MainHeader = props => (
  <ImageBackground
    source={require('../assets/bg.png')}
    style={headerStyle.mainHeaderStyle}
    {...props}
  />
);

export const BackButton = props => (
  <TouchableOpacity
    onPress={props.onPress}
    style={headerStyle.touchBackButton}
    {...props}>
    <Image
      source={require('../images/back.png')}
      style={headerStyle.imageBackButton}
    />
  </TouchableOpacity>
);
export const H1 = props => (
  <Text style={headerStyle.h1} {...props}>
    {props.text}
  </Text>
);
export const H2 = props => (
  <Text style={headerStyle.h2} {...props}>
    {props.text}
  </Text>
);

export const MainView = props => (
  <SafeAreaView style={{flex: 1, backgroundColor: 'white'}} {...props} />
);
export const BackgroundImage = props => (
  <ImageBackground
    source={require('../images/bg.png')}
    style={{flex: 1}}
    {...props}
  />
);

export const SubmitButton = props => (
  <TouchableOpacity
    onPress={props.onPress}
    style={headerStyle.touchSubmitButton}>
    <Text style={headerStyle.textSubmitButton}>{props.title}</Text>
  </TouchableOpacity>
);
export const ForgotPasswordButton = props => (
  <TouchableOpacity
    style={headerStyle.touchForgotPassword}
    onPress={props.onPress}
    {...props}>
    <Text style={headerStyle.textForgotPassword}>Forgot your password?</Text>
  </TouchableOpacity>
);
export const FormView = props => (
  <View style={[headerStyle.viewForm, props.containerStyle]} {...props} />
);

export const RatingView = ({defaultRating, starContainerStyle = {}}) => (
  <AirbnbRating
    count={5}
    defaultRating={defaultRating}
    showRating={false}
    size={18}
    ratingContainerStyle={[
      headerStyle.ratingContainerStyle,
      starContainerStyle,
    ]}
    reviewSize={0}
    isDisabled={true}
    selectedColor={'#FAC917'}
    unSelectedColor={'#C7C7CC'}
  />
);
const headerStyle = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 20,
    paddingTop,
    paddingBottom: 15,
    backgroundColor: '#FBD303',
  },
  title: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 17,
    color: '#09304B',
    paddingHorizontal: 20,
  },
  backImage: {
    width: 12,
    height: 22,
    resizeMode: 'contain',
  },
  touch: {
    padding: 10,
    position: 'absolute',
    left: 10,
    bottom: 5,
  },

  linearGradient: {
    paddingTop,
    paddingBottom: 10,
    paddingHorizontal: 20,
  },
  mainHeaderStyle: {
    padding: 20,
    paddingTop,
  },
  viewHeader: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 15,
  },
  touchBack: {
    position: 'absolute',
    left: 0,
  },
  imageBack: {
    height: 40,
    width: 40,
    resizeMode: 'contain',
  },
  textTitle: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 20,
    color: '#FFFFFF',
    marginHorizontal: 50,
  },
  touchBackButton: {
    padding: 10,
    marginTop: paddingTop,
    marginLeft: 20,
    alignSelf: 'flex-start',
  },
  imageBackButton: {
    width: 40,
    height: 40,
    resizeMode: 'contain',
  },
  h1: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    fontSize: 34,
    color: '#FFFFFF',
    marginHorizontal: 30,
    marginTop: 20,
  },
  h2: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 17,
    color: '#FFFFFF',
    marginVertical: 15,
    marginHorizontal: 30,
  },
  touchSubmitButton: {
    backgroundColor: '#F13268',
    borderColor: 20,
    width: '90%',
    alignSelf: 'center',
    borderRadius: 25,
    padding: 15,
    marginTop: 50,
  },
  textSubmitButton: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 17,
    fontWeight: 'bold',
    color: '#FFFFFF',
    alignSelf: 'center',
    justifyContent: 'center',
  },

  viewForm: {
    backgroundColor: 'white',
    width: '90%',
    borderRadius: 15,
    alignSelf: 'center',
    paddingVertical: 30,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    marginTop: 20,
  },
  touchForgotPassword: {
    margin: 15,
    marginBottom: 5,
    padding: 5,
    alignSelf: 'flex-end',
  },
  textForgotPassword: {
    fontFamily: 'Avenir-Medium',
    fontSize: 15,
    fontWeight: '500',
    color: '#FF2D55',
  },
  ratingContainerStyle: {
    alignItems: 'flex-start',
  },
});

const LinearColor = [
  ['#BD24B3', '#F977D5'],
  ['#F55555', '#FCCF31'],
  ['#AF5A50', '#F3ACA3'],
  ['#28C76F', '#57F8A0'],
  ['#7367F0', '#CE9FFC'],
];

import React from 'react';
import {
  LogBox,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import {Provider} from 'react-redux';
import store from './src/redux/store';
import StackNavigator from './src/screens/navigator/StackNavigator';
window.consolejson = json => console.log(JSON.stringify(json, null, 2));
const App = () => {
  LogBox.ignoreLogs(['Warning:', 'Cannot update a component from inside']);
  LogBox.ignoreAllLogs(true);
  return (
    <Provider store={store}>
      <StackNavigator />
    </Provider>
  );
};

const styles = StyleSheet.create({});

export default App;
